from datetime import datetime

ghg = {
    "by_gsheet_status": {},
    "config": {
        "bu": "asia",
        "dest_folder_failure": "https://drive.google.com/drive/folders/1kOTt6z5s1cu1azKfRIq8r9vCpfjaID0Q",
        "dest_folder_success": "https://drive.google.com/drive/folders/1h9DmEZSpluciG7GsGrPiwyFsFEvc0vHI",
        "error_email": ["fung.yeung@veolia.com", "jean-marc.bonnefoy@veolia.com"],
        "gcs_done_folder": "gs://logsheet_staging_asia_northeast/done/asia/ghg_performance/2022/2/10/{}",
        "gcs_done_path": "gs://logsheet_staging_asia_northeast/done/asia/ghg_performance/2022/2/10/{}",
        "gcs_err_path": "gs://logsheet_staging_asia_northeast/ingested_with_error/asia/ghg_performance/{}",
        "gcs_ingested_with_error_folder": "gs://logsheet_staging_asia_northeast/ingested_with_error/asia/ghg_performance/2022/2/10/{}",
        "gcs_staging_files": [
            "gs://logsheet_staging_asia_northeast/staging/asia/ghg_performance/AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647.csv"
        ],
        "gcs_stg_folder": "gs://logsheet_staging_asia_northeast/staging/asia/ghg_performance",
        "gcs_tmp_files": [
            "gs://logsheet_staging_asia_northeast/tmp/asia/ghg_performance/AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647.csv"
        ],
        "gcs_tmp_path": "gs://logsheet_staging_asia_northeast/tmp/asia/ghg_performance/{}",
        "gsheet_ids": [
            {
                "id": "1z_iptsghcXUQUyjT0ZIGdgKxeP3fhxmDKXP6hZ9jVDQ",
                "name": "AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647",
                "url": "https://docs.google.com/spreadsheets/d/1z_iptsghcXUQUyjT0ZIGdgKxeP3fhxmDKXP6hZ9jVDQ",
            }
        ],
        "gsheet_tab_name": "data",
        "job_id": "ghg_performance__20220210_01h42m00s",
        "mail_on_success": "true",
        "pending_gsheet_ids": {
            "1z_iptsghcXUQUyjT0ZIGdgKxeP3fhxmDKXP6hZ9jVDQ": {
                "gcs_staging_location": "gs://logsheet_staging_asia_northeast/staging/asia/ghg_performance/AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647.csv",
                "name": "AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647",
                "url": "https://docs.google.com/spreadsheets/d/1z_iptsghcXUQUyjT0ZIGdgKxeP3fhxmDKXP6hZ9jVDQ",
            }
        },
        "primary_key": [
            "globalreport_id",
            "business_unit",
            "project_name",
            "site_name",
            "asia_code",
            "global_report_code",
            "indicator_name",
            "indicator_type",
            "date",
        ],
        "sink_ds_name": "tests",
        "sink_location": "asia-northeast1",
        "sink_location_display_name": "asia_northeast",
        "sink_project_name": "cn-ops-spdigital",
        "sink_schema": {
            "activity": "STRING",
            "activity_type": "STRING",
            "asia_code": "STRING",
            "business_line": "STRING",
            "business_unit": "STRING",
            "comment": "STRING",
            "country": "STRING",
            "data_owner_email": "STRING",
            "date": "DATE",
            "file_id": "STRING",
            "formula": "FLOAT",
            "global_report_code": "STRING",
            "globalreport_id": "STRING",
            "indicator_name": "STRING",
            "indicator_type": "STRING",
            "insertion_time": "TIMESTAMP",
            "interaction_w_other_reports": "STRING",
            "is_applicable": "STRING",
            "period": "STRING",
            "project_name": "STRING",
            "raw_data_availability": "STRING",
            "scope": "STRING",
            "sheet_name": "STRING",
            "site_name": "STRING",
            "sub_activity": "STRING",
            "time_step": "STRING",
            "unit": "STRING",
            "value": "FLOAT",
        },
        "sink_tbl": "cn-ops-spdigital.tests.ghg_performance",
        "sink_tbl_name": "ghg_performance",
        "site": "ghg_performance",
        "src_folder": "https://drive.google.com/drive/folders/1MrFany3mlMhhcZbmZ6buzVxpgWGMBCdX",
        "stg_ds_name": "logsheet_staging_asia_northeast",
        "stg_project_name": "cn-ops-spdigital",
        "stg_tbl": "cn-ops-spdigital.logsheet_staging_asia_northeast.ghg_performance",
        "stg_tbl_name": "ghg_performance",
        "timezone": "Asia/Hong_Kong",
    },
    "err_msg": 'Traceback (most recent call last):\n File "/workspace/main.py", line 50, in main\n output: tuple = ingest_drive_logsheet(\n File "/workspace/main.py", line 140, in ingest_drive_logsheet\n create_or_replace_upsert_sporc(upsert_statment=upsert_statement, **config)\n File "/workspace/bq_ops/ingestion.py", line 419, in create_or_replace_upsert_sporc\n bq.create_routine(routine, exists_ok=True)\n File "/layers/google.python.pip/pip/lib/python3.9/site-packages/google/cloud/bigquery/client.py", line 665, in create_routine\n api_response = self._call_api(\n File "/layers/google.python.pip/pip/lib/python3.9/site-packages/google/cloud/bigquery/client.py", line 760, in _call_api\n return call()\n File "/layers/google.python.pip/pip/lib/python3.9/site-packages/google/api_core/retry.py", line 283, in retry_wrapped_func\n return retry_target(\n File "/layers/google.python.pip/pip/lib/python3.9/site-packages/google/api_core/retry.py", line 190, in retry_target\n return target()\n File "/layers/google.python.pip/pip/lib/python3.9/site-packages/google/cloud/_http.py", line 479, in api_request\n raise exceptions.from_http_response(response)\ngoogle.api_core.exceptions.BadRequest: 400 POST https://bigquery.googleapis.com/bigquery/v2/projects/cn-ops-spdigital/datasets/logsheet_staging_asia_northeast/routines?prettyPrint=false: Syntax error: Unexpected "," at [29:18]\n',
    "fail_files": {
        "1z_iptsghcXUQUyjT0ZIGdgKxeP3fhxmDKXP6hZ9jVDQ": {
            "drive_url": "https://docs.google.com/spreadsheets/d/1z_iptsghcXUQUyjT0ZIGdgKxeP3fhxmDKXP6hZ9jVDQ",
            "err_msg": [
                {
                    "location": "query",
                    "message": "Could not parse 'emission factor kg CO2 eq/ unit/km' as DOUBLE for field formula (position 18) starting at location 173467 with message 'Unable to parse'",
                    "reason": "invalidQuery",
                },
                {
                    "location": "query",
                    "message": "Could not parse 'emission factor kg CO2 eq/ unit/km' as DOUBLE for field formula (position 18) starting at location 174986 with message 'Unable to parse'",
                    "reason": "invalidQuery",
                },
                {
                    "location": "query",
                    "message": "Could not parse 'emission factor kg CO2 eq/ unit/km' as DOUBLE for field formula (position 18) starting at location 176503 with message 'Unable to parse'",
                    "reason": "invalidQuery",
                },
                {
                    "location": "query",
                    "message": "Could not parse 'emission factor kg CO2 eq/ unit/km' as DOUBLE for field formula (position 18) starting at location 178019 with message 'Unable to parse'",
                    "reason": "invalidQuery",
                },
                {
                    "location": "query",
                    "message": "Could not parse 'emission factor kg CO2 eq/ unit/km' as DOUBLE for field formula (position 18) starting at location 185368 with message 'Unable to parse'",
                    "reason": "invalidQuery",
                },
            ],
            "err_row_cnt": 9,
            "err_stage": "GCS to BQ Staging",
            "gcs_staging_location": "gs://logsheet_staging_asia_northeast/staging/asia/ghg_performance/AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647.csv",
            "gcs_url": "https://storage.cloud.google.com/logsheet_staging_asia_northeast/ingested_with_error/asia/ghg_performance/AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647.csv",
            "gsheet_row_cnt": 635,
            "name": "AsiaBSPEnvMonthlyRpt_Ecospace_20220208-173647",
            "staging_num_rows": 626,
        }
    },
    "ingest_state": "unknown_error",
    "success_files": {},
    "url": {
        "bq_sink_tbl": "https://console.cloud.google.com/bigquery?d=tests&p=cn-ops-spdigital&t=ghg_performance&page=table",
        "bq_stg_tbl": "https://console.cloud.google.com/bigquery?d=logsheet_staging_asia_northeast&p=cn-ops-spdigital&t=ghg_performance&page=table",
        "bq_upsert_statment": "https://console.cloud.google.com/bigquery?d=logsheet_staging_asia_northeast&p=cn-ops-spdigital&page=routine&r=ghg_performance_ghg_performance",
        "gcs_error_folder": "https://console.cloud.google.com/storage/browser/logsheet_staging_asia_northeast/ingested_with_error/asia/ghg_performance",
        "gcs_success_folder": "https://console.cloud.google.com/storage/browser/logsheet_staging_asia_northeast/done/asia/ghg_performance/2022/2/10",
    },
}

showcase = {
    "bu": "kr",
    "dest_folder_failure": "https://drive.google.com/drive/folders/1W8s_bg-n0_Y9xuonh8aoqUSQDHF7OPX3",
    "dest_folder_success": "https://drive.google.com/drive/folders/1zNb83aUrRnChsejfXu83i_crKleQ0vhE",
    "error_email": ["fung.yeung@veolia.com"],
    "gsheet_ids": [
        {
            "id": "1Urv_fJ8O_SZN2BAizNt0XMihP7cq_g05nD6_FpUNJw0",
            "name": "Copy of Copy of Logsheet-SeoulStMaryHospital-20210701-20220102_20220103125359",
        },
        # {
        #     "id": "1DXSn2H7xM5L32HJjR92b_s7U1CjSipPSiIVfouLw5gI",
        #     "name": "Copy of Copy of Logsheet-SeoulSeonamHospital-20211225-20220101_20220102205544",
        # },
        # {
        #     "id": "1iw3Q7PXI83T6c6t5Cpa9Gjl2ZW6kTlBTL74D9k9PvPQ",
        #     "name": "Copy of Copy of Logsheet-SeoulStMaryHospital-20210701-20220102_20220103125152",
        # },
        # {
        #     "id": "1vyMh1NXn6oeS6-V0X45L1cy3nfZLvWwnVFyfVLrVRpQ",
        #     "name": "Copy of Copy of Logsheet-SeoulSeonamHospital-20211226-20220102_20220103072802",
        # },
        # {
        #     "id": "1YqfUEe9SYZWCKFrO8usaaCxmx5NUYvKWHpp_zJBRvtk",
        #     "name": "Copy of Copy of Logsheet-SeoulStMaryHospital-20210701-20220102_20220103082304",
        # },
        # {
        #     "id": "1L6nh2ojdyUTSAjfOSrHw__Y0ZfuVCFys6tOyVaNJKSQ",
        #     "name": "Copy of Copy of Logsheet-SeoulSeonamHospital-20211225-20220101_20220102073454",
        # },
        # {
        #     "id": "11hCBWvfZOw2hZt_qSvoTOJRfXKd6pegeJtuUmbhN2CU",
        #     "name": "Copy of Copy of Logsheet-IlsanPaikHospital-20211222-20211229_20211230083412",
        # },
        # {
        #     "id": "1TpzZAcngtl_lewICBSf2WOAAL6fF0wual8zA3vDgJs4",
        #     "name": "Copy of Copy of Logsheet-IlsanPaikHospital-20211208-20211215_20211216085821",
        # },
        # {
        #     "id": "1Ol_Il2U-KT9NOxr0dP0ekIeIfNeezInOPdxSWvFPrBc",
        #     "name": "Copy of Copy of Logsheet-IlsanPaikHospital-20211215-20211222_20211223092158",
        # },
    ],
    "gsheet_tab_name": "data",
    "mail_on_success": True,
    "primary_key": ["measure_time", "measure_point"],
    "sink_tbl": "cn-ops-spdigital.tests.showcase_tbl",
    "site": "showcase",
    "src_folder": "https://drive.google.com/drive/folders/14WgtI2_TUTDZrAANS4fodWIgvAYn1YsL",
    "timezone": "Asia/Seoul",
}
site1 = {
    "error_email": ["fung.yeung@veolia.com"],
    "primary_key": ["measure_time", "measure_point"],
    "gsheet_tab_name": "data",
    "dest_folder_success": "https://drive.google.com/drive/folders/1zNb83aUrRnChsejfXu83i_crKleQ0vhE",
    "dest_folder_failure": "https://drive.google.com/drive/folders/1W8s_bg-n0_Y9xuonh8aoqUSQDHF7OPX3",
    "bu": "kr",
    "timezone": "Asia/Seoul",
    "mail_on_success": True,
    "src_folder": "https://drive.google.com/drive/folders/1KDxtR14hAuXsbazZYmDiqaX2LiZwsqyx",
    "sink_tbl": "cn-ops-spdigital.tests.uat_site_1",
    "site": "site2",
    # "gsheet_ids": [
    #     {
    #         "id": "1ngHRWhz1_YTw3IB0GtlYqu_0SybJ11dCiw9BMj-0AJg",
    #         "name": "logsheet_in_site1",
    #     }
    # ],
    "gsheet_ids": [
        {
            "id": "1ngHRWhz1_YTw3IB0GtlYqu_0SybJ11dCiw9BMj-0AJg",
            "name": "logsheet_in_site1",
        },
        # {
        #     "id": "1-n031PhpIkOVhCfEou9jqDXWUMQyWe06Th6DDI9SuFE",
        #     "name": "workshop_logsheet",
        # },
    ],
}

refential = {
    "gsheet_tab_name": "data",
    "error_email": ["fung.yeung@veolia.com"],
    "primary_key": ["business_unit", "site_name", "project", "site_type"],
    "dest_folder_success": "https://drive.google.com/drive/folders/1zNb83aUrRnChsejfXu83i_crKleQ0vhE",
    "dest_folder_failure": "https://drive.google.com/drive/folders/1W8s_bg-n0_Y9xuonh8aoqUSQDHF7OPX3",
    "bu": "kr",
    "timezone": "Asia/Seoul",
    "mail_on_success": True,
    "src_folder": "https://drive.google.com/drive/folders/1FbFIvuhBSmrBWTkvMkQuHlTq3sUhxEOS",
    "sink_tbl": "cn-ops-spdigital.tests.newlogsheets_test_ref_asia_site",
    "site": "site1",
    "gsheet_ids": [
        {
            "id": "1zvAFF2ffyPpktZYkUanNStBP6zjMuD_ossd3ZmHLMPc",
            "name": "jmtest2_referential",
        }
    ],
}


action_plan_test = {
    "gsheet_tab_name": "data",
    "error_email": ["fung.yeung@veolia.com"],
    "primary_key": ["project", "bi", "no", "year"],
    "dest_folder_success": "https://drive.google.com/drive/folders/1zNb83aUrRnChsejfXu83i_crKleQ0vhE",
    "dest_folder_failure": "https://drive.google.com/drive/folders/1W8s_bg-n0_Y9xuonh8aoqUSQDHF7OPX3",
    "bu": "kr",
    "timezone": "Asia/Seoul",
    "mail_on_success": True,
    "src_folder": "https://drive.google.com/drive/folders/1KDxtR14hAuXsbazZYmDiqaX2LiZwsqyx",
    "sink_tbl": "cn-ops-spdigital.tests_region_EU.action_plan_test",
    "site": "action_plan_test",
    "gsheet_ids": [
        {
            # "id": "1ngHRWhz1_YTw3IB0GtlYqu_0SybJ11dCiw9BMj-0AJg",
            # "name": "logsheet_in_site1",
            "id": "1IKNG9Nv_OMgaY3JZ1tUPUepanFAd6DdRQqgW6KWgozM",
            "name": "action_plan_test_file1_01",
        }
    ],
}


now = datetime.utcnow().strftime("%Y%m%d_%Hh%Mm%Ss")
enriched_dev_config = {
    "bu": "kr",
    "timezone": "Asia/Seoul",
    "dest_folder": "https://drive.google.com/drive/folders/19Vc7x2uR_ypDaH2V93yxjx21hDsc3Tbj",
    "error_email": ["fung.yeung@veolia.com"],
    "gsheet_ids": [
        {
            "id": "1ngHRWhz1_YTw3IB0GtlYqu_0SybJ11dCiw9BMj-0AJg",
            "name": "awesome_logsheet",
        }
    ],
    "primary_key": ["measure_time", "measure_point"],
    "sink_tbl": "cn-ops-spdigital.tests.data_asia_raw_awesome_site",
    "site": "site1",
    "src_folder": "https://drive.google.com/drive/folders/1KDxtR14hAuXsbazZYmDiqaX2LiZwsqyx",
    "sink_project_name": "cn-ops-spdigital",
    "sink_ds_name": "tests",
    "sink_tbl_name": "data_asia_raw_awesome_site",
    # "sink_location": "asia-northeast",
    "sink_location": "asia-northeast2",
    "sink_location_display_name": "asia_northeast",
    "stg_project_name": "cn-ops-spdigital",
    "stg_ds_name": "logsheet_staging_asia_northeast",
    "stg_tbl_name": "data_asia_raw_awesome_site",
    "stg_tbl": "cn-ops-spdigital.logsheet_staging_asia_northeast.data_asia_raw_awesome_site",
    "job_id": f"site1__{now}",
}
