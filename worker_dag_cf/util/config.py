from datetime import datetime
from pathlib import PurePosixPath
from typing import Tuple
import re
import logging

logger = logging.getLogger(__name__)

import google.auth
from google.cloud import bigquery


# def get_sink_tbl_location(config: dict) -> Tuple[str, str]:
def get_sink_info(config: dict) -> Tuple[str, str, dict]:

    # Sink Dataset info
    credentials, _ = google.auth.default(
        scopes=[
            "https://www.googleapis.com/auth/drive",
            "https://www.googleapis.com/auth/bigquery",
        ]
    )
    sink_client = bigquery.Client(
        credentials=credentials, project=config["sink_project_name"]
    )
    sink_ds = sink_client.get_dataset(
        f"{config['sink_project_name']}.{config['sink_ds_name']}"
    )
    sink_ds_location_display_name = re.sub(r"\d", "", sink_ds.location)
    sink_ds_location_display_name = re.sub(
        r"[^a-zA-Z_]", "_", sink_ds_location_display_name
    )
    sink_ds_location_display_name = sink_ds_location_display_name.lower()
    # return sink_ds.location, sink_ds_location_display_name

    # Sink Table info
    sink_tbl = sink_client.get_table(config["sink_tbl"])
    # sink_tbl.schema
    return (
        sink_ds.location,
        sink_ds_location_display_name,
        {c.name: c.field_type for c in sink_tbl.schema},
    )


def get_enriched_config(config: dict = None, request=None) -> dict:

    # create tacking dict to keep ingestion state for each of the gsheet files
    # downstream process should loop over this key to continue ingestion as
    # failure one would be taken out from here
    # key: gsheet_file_id, val: dict of relvant info

    # bq related info
    (
        config["sink_project_name"],
        config["sink_ds_name"],
        config["sink_tbl_name"],
    ) = config["sink_tbl"].split(".")

    (
        config["sink_location"],
        config["sink_location_display_name"],
        config["sink_schema"],
    ) = get_sink_info(config)

    config["stg_project_name"] = "cn-ops-spdigital"
    config["stg_ds_name"] = "logsheet_staging_{}".format(
        config["sink_location_display_name"]
    )
    # staging table need to be unique as there can be multiple YAML key point to the same sink table
    config["stg_tbl_name"] = config["site"] + "_" + config["sink_tbl_name"]
    config["stg_tbl"] = "{}.{}.{}".format(
        config["stg_project_name"], config["stg_ds_name"], config["stg_tbl_name"]
    )

    # gcs related info
    # <bu> instead of <site> is used for folder structure since it provide more
    # clarity

    config[
        "gcs_stg_folder"
    ] = f"gs://logsheet_staging_{config['sink_location_display_name']}/staging/{config['bu']}/{config['site']}"
    config[
        "gcs_tmp_path"
    ] = f"{config['gcs_stg_folder'].replace('/staging/', '/tmp/')}/{{}}"

    gcs_stg_folder = config["gcs_stg_folder"]
    staging_files_gcs_path = [
        f"{gcs_stg_folder}/{f['name']}.csv" for f in config["gsheet_ids"]
    ]
    # raw data + gcs_path metadata column
    config["gcs_staging_files"] = staging_files_gcs_path
    # raw data
    config["gcs_tmp_files"] = [
        f.replace("/staging/", "/tmp/") for f in config["gcs_staging_files"]
    ]
    now = datetime.utcnow()
    done_folder = str(
        PurePosixPath(gcs_stg_folder.replace("/staging/", "/done/"))
        / str(now.year)
        / str(now.month)
        / str(now.day)
        / "{}"
    )
    done_folder = done_folder.replace("gs:/", "gs://")
    config["gcs_done_folder"] = done_folder

    done_path = str(
        PurePosixPath(gcs_stg_folder.replace("/staging/", "/done/"))
        / str(now.year)
        / str(now.month)
        / str(now.day)
        / "{}"
    )
    done_path = done_path.replace("gs:/", "gs://")
    config["gcs_done_path"] = done_path  # + "/{}.csv"
    config["gcs_err_path"] = config["gcs_tmp_path"].replace(
        "/tmp/", "/ingested_with_error/"
    )

    config["gcs_ingested_with_error_folder"] = config["gcs_done_folder"].replace(
        "/done/", "/ingested_with_error/"
    )
    # config["gcs_ingested_with_error_folder"] = config["gcs_done_folder"].replace(
    #     "/done/", "/ingested_with_error/"
    # )
    # unique job run id for both airflow DAG and Bigquery job
    now = now.strftime("%Y%m%d_%Hh%Mm%Ss")
    config["job_id"] = f"{config['site']}__{now}"

    # adding url for pending_gsheet_ids (later moved to success / fail files)
    config["pending_gsheet_ids"] = {
        gsheet["id"]: {
            "name": gsheet["name"],
            "gcs_staging_location": f"{gcs_stg_folder}/{gsheet['name']}.csv",
            "url": f'https://docs.google.com/spreadsheets/d/{gsheet["id"]}',
        }
        for gsheet in config["gsheet_ids"]
    }

    # add back gsheet url
    for gsheet_id in config["gsheet_ids"]:
        gsheet_id["url"] = f'https://docs.google.com/spreadsheets/d/{gsheet_id["id"]}'

    # {
    #     gsheet["id"]: {
    #         "name": gsheet["name"],
    #         "gcs_staging_location": f"{gcs_stg_folder}/{gsheet['name']}.csv",
    #         'url': f'https://docs.google.com/spreadsheets/d/{gsheet["id"]}'
    #     }
    #     for gsheet in config["gsheet_ids"]
    # }

    return config


if __name__ == "__main__":
    import sys
    from pathlib import Path

    from rich import print

    cwd = Path.cwd()
    sys.path.append(str(cwd / "worker_dag_cf"))
    sys.path.append(str("/home/fung/Documents/dev/gsheet_connector"))
    from util.dev_config_asset import site1

    config = get_enriched_config(site1)
    print(config)
