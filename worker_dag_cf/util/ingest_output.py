def get_init_ingest_output(config: dict):
    ingest_output = {}
    ingest_output["config"] = config
    ingest_output["fail_files"] = {}
    ingest_output["success_files"] = {}

    gcs_stg_folder = config["gcs_stg_folder"]
    gcs_done_folder = config["gcs_done_folder"]
    gcs_ingested_with_error_folder = config["gcs_ingested_with_error_folder"]

    ingest_output["by_gsheet_status"] = {
        gsheet["id"]: {
            "name": gsheet["name"],
            "gcs_staging_location": f"{gcs_stg_folder}/{gsheet['name']}.csv",
            "drive_url": gsheet["url"]
            # "gcs_done_location": f"{gcs_done_folder}/{gsheet['name']}.csv",
            # "gcs_fail_location": f"{gcs_ingested_with_error_folder}/{gsheet['name']}.csv",
        }
        for gsheet in config["gsheet_ids"]
    }

    # get url for GCS, BQ tables Upsert statement
    ingest_output["url"] = {}
    url = ingest_output["url"]

    url["gcs_success_folder"] = (
        ingest_output["config"]["gcs_done_path"]
        .replace("gs://", "https://console.cloud.google.com/storage/browser/")
        .replace("/{}", "")
    )
    url["gcs_error_folder"] = (
        ingest_output["config"]["gcs_err_path"]
        .replace("gs://", "https://console.cloud.google.com/storage/browser/")
        .replace("/{}", "")
    )

    url[
        "bq_upsert_statment"
    ] = f"https://console.cloud.google.com/bigquery?d={config['stg_ds_name']}&p={config['stg_project_name']}&page=routine&r={config['site']}_{config['sink_tbl_name']}"

    # https://console.cloud.google.com/bigquery?d={sink_dataset}&p={sink_project}&t={sink_tbl}&page=table
    table_url = "https://console.cloud.google.com/bigquery?d={d}&p={p}&t={t}&page=table"

    url["bq_stg_tbl"] = table_url.format(
        d=config["stg_ds_name"],
        p=config["stg_project_name"],
        t=config["stg_tbl_name"],
    )
    url["bq_sink_tbl"] = table_url.format(
        d=config["sink_ds_name"],
        p=config["sink_project_name"],
        t=config["sink_tbl_name"],
    )
    # url[
    #     "bq_sink_tbl" = f"https://console.cloud.google.com/bigquery?d={config['sink_ds_name']}&p={config['sink_project_name']}&t={sink_tbl}&page=table"
    # ]

    return ingest_output


def mv_pending_to_fail(gsheet_id: str, ingest_output: dict):
    """move by_gsheet_status's gsheet id to fail files section for later processing

    Args:
        gsheet_id (str): [description]
        ingest_output (dict): [description]
    """

    ingest_output["fail_files"][gsheet_id] = ingest_output["by_gsheet_status"][
        gsheet_id
    ]
    del ingest_output["by_gsheet_status"][gsheet_id]
    return ingest_output


def to_url(ingest_output: dict) -> dict:
    """turn items in ingest_output at the end of the ingestion function (whether it is success or fail) to url for easy navagation in email url

    key to turn into url
        1. gsheet_id
        2. failed_gsheet_id
        3. google cloud storage folder url
        4. bigquery table

    Args:
        ingest_output (dict): [description]

    Returns:
        dict: ingest_output with item becomes url
    """


if __name__ == "__main__":
    from rich import print
    from util.dev_config_asset import site1, showcase
    from util.config import get_enriched_config

    config = get_enriched_config(showcase)
    io = get_init_ingest_output(config=config)
    # add gsheet_url for gsheet-id in success or failed files

    print(io)
    # gsheet_ids
    # https://docs.google.com/spreadsheets/d/{gsheet_id}

    # GCS
    # success case too

    # https://console.cloud.google.com/storage/browser/logsheet_staging_asia_northeast/ingested_with_error/kr/showcase
    # BQ
    # https://console.cloud.google.com/bigquery?d={sink_dataset}&p={sink_project}&t={sink_tbl}&page=table

    # upsert statment
    # https://console.cloud.google.com/bigquery?d=logsheet_staging_asia_northeast&p=cn-ops-spdigital&page=routine&r=showcase_showcase_tbl
