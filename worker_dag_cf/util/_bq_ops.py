from collections import namedtuple
import re
import logging

from google.cloud import bigquery
import google.auth


def create_staging_dataset_if_not_exist(config) -> bigquery.Dataset:
    """create staging dataset in same location as sink dataset if it isn't
    already exist

    Return:
        Staging Dataset Bigquery object
    """
    sink_client, stg_client = config["sink_bq"], config["stg_bq"]
    sink_ds = sink_client.get_dataset(
        f"{config['sink_project_name']}.{config['sink_ds_name']}"
    )

    # string wraggling to get to acceptable format for dataset_name
    sink_ds_location = sink_ds.location
    sink_ds_location = re.sub(r"\d", "", sink_ds_location)
    sink_ds_location = re.sub(r"[^a-zA-Z_]", "_", sink_ds_location)
    stg_ds_name = f"cn-ops-spdigital.logsheet_staging_{sink_ds_location}"
    stg_dataset = bigquery.Dataset(stg_ds_name)

    stg_dataset.location = sink_ds.location
    stg_dataset = stg_client.create_dataset(stg_dataset, exists_ok=True)

    logging.info(
        f'staging dataset "{stg_ds_name}" is available, location: "{sink_ds_location}"'
    )
    return stg_ds_name


def gcs_to_stg(config):
    """ingest logsheet CSV from GCS to BigQuery staging dataset in Asia

        1. Fetch sink table schema
        2. Load GCS CSV to Staging table using sink schema

    Args:
        config ([type]): [description]
    """

    # create staging table object with same table name as sink table
    # TODO: may need to cater for parallel run, i.e.

    stg_client = config["stg_bq"]
    sink_client = config["sink_bq"]

    sink_tbl = sink_client.get_table(config["sink_tbl"])
    sink_schema = sink_tbl.schema

    sink_location_display_name = config["sink_location_display_name"]

    job_config = bigquery.LoadJobConfig(
        skip_leading_rows=1,
        source_format=bigquery.SourceFormat.CSV,
        create_disposition="CREATE_IF_NEEDED",
        write_disposition="WRITE_TRUNCATE",
        schema=sink_schema,
        max_bad_records=100000,
    )

    site = config["site"]
    source_uri = f"gs://logsheet_staging_{sink_location_display_name}/staging/{site}/*"

    stg_project_id = "cn-ops-spdigital"
    stg_dataset_id = "logsheet_staging_" + config["sink_location_display_name"]
    stg_table_name = (
        config["sink_tbl_name"] + "_" + config["site"]
    )  # trailing config key must be included to avoid multiple keys pointing to same sink table replacing each other in parallel run
    stg_full_table_name = f"{stg_project_id}.{stg_dataset_id}.{stg_table_name}"

    stg_client.load_table_from_uri(
        source_uris=source_uri,
        destination=stg_full_table_name,  # tests.<tbl_name>
        job_config=job_config,
        job_id_prefix=site,  # config.site
    )
    logging.info(
        f'CSV files "{source_uri}" ingested to staging table "{stg_full_table_name}"'
    )

    pass


def bigquery_ingest(config, credentials):
    """entry point of bigquery ops
        1. create staging table if not exists (might not need)
        2. ingest from GCS to BQ

    Args:
        config ([type]): [description]
        credentials ([type]): [description]
    """
    stg_client = bigquery.Client(credentials=credentials, project="cn-ops-spdigital")
    sink_client = bigquery.Client(
        credentials=credentials, project=config.sink_project_name
    )

    stg_dataset_name = create_staging_dataset_if_not_exist(
        config, stg_client, sink_client
    )
    # create_stg_table_if_not_exists(config, stg_client, sink_client, stg_dataset_name)

    gcs_to_stg(config, stg_client, sink_client, stg_dataset_name)

    # stg_to_sink()


if __name__ == "__main__":
    import sys
    from pathlib import Path
    from rich import print
    from worker_dag_cf.util.config import get_enriched_config

    logging.basicConfig(level=logging.INFO)

    cwd = Path.cwd()
    sys.path.append(cwd / "worker_dag_cf/util")
    sys.path.append(cwd / "sensor_dag_cf/util")

    config = {
        "timezone": "Asia/Hong_Kong",
        "error_email": ["fung.yeung@veolia.com"],
        "gsheet_ids": [
            {
                "id": "1ngHRWhz1_YTw3IB0GtlYqu_0SybJ11dCiw9BMj-0AJg",
                "name": "site1_logsheet1",
            },
            # {
            #     "id": "17UzmAQHsDI9kLaeEzB6mN25QTSXQEBNUr3_R4jHk6TA",
            #     "name": "site1_logsheet2",
            # },
        ],
        "primary_key": ["measure_time", "measure_point"],
        "sink_tbl": "cn-ops-spdigital.tests.data_asia_raw_site2",
        "src_folder": "https://drive.google.com/drive/folders/1X0K8_32VGnCcEJSPLovE1E4JqzmFMrKv",
        "site": "site2",
    }
    c = get_enriched_config(config)
    print(c)
    create_staging_dataset_if_not_exist(c)
    gcs_to_stg(c)
    # config = get_enriched_config(CONFIG)

    # credentials, _ = google.auth.default(
    #     scopes=[
    #         "https://www.googleapis.com/auth/drive",
    #         "https://www.googleapis.com/auth/bigquery",
    #     ]
    # )
    # # print(c)

    # bigquery_ingest(c, credentials)
