# TODO: add clean up operations for gcs and drive
from datetime import datetime
from pathlib import Path
import re
import logging

logger = logging.getLogger(__name__)
import google.auth
from google.cloud import storage
from googleapiclient.discovery import build
from google.api_core.exceptions import NotFound
from googleapiclient.errors import HttpError


def move_drive_files_to_dest_folder(
    file_id: str, src_folder: str, dest_folder: str, **kwargs
):
    """move files from gdrive staging folder to dest folder

    Args:
        file_id (str): [description]
        src_folder (str): [description]
        dest_folder (str): [description]
    """
    SCOPES = [
        "https://www.googleapis.com/auth/drive",
        "https://www.googleapis.com/auth/spreadsheets",
        "https://www.googleapis.com/auth/cloud-platform",
    ]
    credentials, _ = google.auth.default(scopes=SCOPES)
    service = build("drive", "v3", credentials=credentials, cache_discovery=False)

    gdrive_prefix = "https://drive.google.com/drive/folders/"
    src_folder_id, dest_folder_id = src_folder.replace(
        gdrive_prefix, ""
    ), dest_folder.replace(gdrive_prefix, "")
    service.files().update(
        fileId=file_id,
        addParents=dest_folder_id,
        removeParents=src_folder_id,
        fields="id, parents",
    ).execute()
    logger.info(
        f'moved drive file "{file_id}" from src_folder:"{src_folder}" to dest_folder:"{dest_folder}"'
    )


def gcs_rm(full_gcs_path: str) -> None:
    """remove a single gcs blob

        bucket name only allows alphanumeric characters and underscore (_)
    Args:
        full_gcs_path (str): fully qualified gcs_path for a file
    """
    # gs://logsheet_staging_asia_northeast/staging/site1
    if not full_gcs_path.startswith("gs://"):
        full_gcs_path = "gs://" + full_gcs_path

    bucket_name = re.match("gs://([a-zA-Z0-9_]*)/.*", full_gcs_path).group(1)
    blob_path = full_gcs_path.replace(f"gs://{bucket_name}/", "")
    gcs = storage.Client()
    bucket = gcs.get_bucket(bucket_or_name=bucket_name)
    bucket.delete_blob(blob_path)
    logger.info(f'deleted gcs file "{full_gcs_path}"')


def gcs_mv(src_gcs_path: str, dest_gcs_path: str):
    """move file from one gcs location to another within the same bucket


    Args:
        src_gcs_path (str): [description]
        dest_gcs_path (str): [description]
    """

    gcs = storage.Client()
    bucket_name = re.match("gs://([a-zA-Z0-9_]*)/.*", src_gcs_path).group(1)
    b = gcs.bucket(bucket_name)
    src_blob_path = src_gcs_path.replace(f"gs://{bucket_name}/", "")
    dest_blob_path = dest_gcs_path.replace(f"gs://{bucket_name}/", "")

    src_blob = b.blob(src_blob_path)
    dest_blob = b.rename_blob(src_blob, dest_blob_path)
    logger.info(f"moved file from {src_blob.name} to {dest_blob.name}")
    # gs://logsheet_staging_asia_northeast/done/site1/<year>/<month>/<date>/<filename>


def after_ingestion_file_cleanup(
    ingest_output: dict,
) -> None:
    """Do the required files ops on gdrive and gcs base on output response code,
    couple of try and except block were used so that we do not need to know the
    exact combination of which files is in where. As long as we guarantee the final
    files location are in the right place base on ingestion response.

    <regardless_of_success_and_failure>
        gcs: delete files in /<staging>/<region>/<YAML_key>

    <success_case>
        gdrive: gsheet files move from <src_folder> to <dest_folder_success>
        gcs: move files from /tmp to /done/<region>/<YAML_key>/<yr>/<mth>/<day> (UTC
        time)
    <failure_case>
        gdrive: gsheet files move from <src_folder> to <dest_folder_failure>
        gcs: move files from /tmp to /ingested_with_error/<region>/<YAML_key>


        Args:
            config (dict): [description]
    TODO: this clean up files operation should have done in airflow DAG in as another task base on "ingest_output, to be optimzed"
    """

    # config might not be completed
    config = ingest_output["config"]
    gcs_tmp_path = config.get("gcs_tmp_path")
    gcs_done_path = config.get("gcs_done_path")
    all_gsheet_id = [gsheet_info["id"] for gsheet_info in config.get("gsheet_ids")]
    print(f"all_gsheet_id: {all_gsheet_id}")

    if stg_files := config.get("gcs_staging_files"):
        try:
            # whatever the run result staging files in GCS need to be removed
            for f in stg_files:
                gcs_rm(f)
        except NotFound:
            logger.info(
                f"there are no CSV files '{f}' in GCS staging folder to clean up!"
            )

    # # if ingest output is failure or partially ingested, consider it failure
    # failure = output[1] >= 400 or output[1] == 207
    # # move GDrive files to appropriate location base on ingestion output
    # success_files = [v['name']for v in ingest_output['success_files'].values()]

    # success cases handling
    dest_folder_success = config.get("dest_folder_success")
    src_folder = config.get("src_folder")

    if success_files := ingest_output.get("success_files"):
        for gsheet_id, gsheet_info in success_files.items():
            all_gsheet_id.remove(gsheet_id)
            # drive
            try:
                move_drive_files_to_dest_folder(
                    gsheet_id,
                    src_folder=src_folder,
                    dest_folder=dest_folder_success,
                )
            except HttpError as e:
                if "Increasing the number of parents is not allowed" in str(e):
                    logger.info(
                        'The Gsheet files are no longer in "src_folder" locations!'
                    )
                else:
                    raise

            # GCS
            gsheet_fn_name = gsheet_info["name"] + ".csv"

            gsheet_info["gcs_url"] = (
                config["gcs_done_path"]
                .format(gsheet_fn_name)
                .replace("gs://", "https://storage.cloud.google.com/")
            )
            src_path = gcs_tmp_path.format(gsheet_fn_name)
            dest_path = gcs_done_path.format(gsheet_fn_name)
            try:
                gcs_mv(src_gcs_path=src_path, dest_gcs_path=dest_path)
            except NotFound:
                logger.info(
                    "there are no CSV files in GCS tmp folder to move to other foder"
                )

    # failure case handling
    dest_folder_failure = config.get("dest_folder_failure")
    gcs_err_path = config.get("gcs_err_path")

    if fail_files := ingest_output.get("fail_files"):
        for gsheet_id, gsheet_info in fail_files.items():
            all_gsheet_id.remove(gsheet_id)
            # drive
            try:
                move_drive_files_to_dest_folder(
                    gsheet_id,
                    src_folder=src_folder,
                    dest_folder=dest_folder_failure,
                )
            except HttpError as e:
                if "Increasing the number of parents is not allowed" in str(e):
                    logger.info(
                        'The Gsheet files are no longer in "src_folder" locations!'
                    )
                else:
                    raise

            # GCS
            gsheet_fn_name = gsheet_info["name"] + ".csv"
            # add download path back to ingest_output
            gsheet_info["gcs_url"] = (
                config["gcs_err_path"]
                .format(gsheet_fn_name)
                .replace("gs://", "https://storage.cloud.google.com/")
            )

            src_path = gcs_tmp_path.format(gsheet_fn_name)
            dest_path = gcs_err_path.format(gsheet_fn_name)
            try:
                gcs_mv(src_gcs_path=src_path, dest_gcs_path=dest_path)
            except NotFound:
                logger.info(
                    "there are no CSV files in GCS tmp folder to move to other foder"
                )

    # When CF crashed. enriched config might not be built at all, so do a last lookup to move all the file from drive src_folder to failure folder at this point
    if all_gsheet_id:
        print(f"there are still gsheet_id not catered: {all_gsheet_id}")
        for gsheet_id in all_gsheet_id:
            try:
                move_drive_files_to_dest_folder(
                    gsheet_id,
                    src_folder=src_folder,
                    dest_folder=dest_folder_failure,
                )
            except HttpError as e:
                if "Increasing the number of parents is not allowed" in str(e):
                    logger.info(
                        'The Gsheet files are no longer in "src_folder" locations!'
                    )
                else:
                    raise

    # if failure:
    #     drive_dest_folder = config["dest_folder_failure"]
    # else:
    #     drive_dest_folder = config["dest_folder_success"]

    # from googleapiclient.errors import HttpError

    # try:
    #     for gsheet in config["gsheet_ids"]:
    #         move_drive_files_to_dest_folder(
    #             gsheet["id"],
    #             src_folder=config["src_folder"],
    #             dest_folder=drive_dest_folder,
    #         )
    # except HttpError as e:
    #     if "Increasing the number of parents is not allowed" in str(e):
    #         logger.info('The Gsheet files are no longer in "src_folder" locations!')
    #     else:
    #         raise

    # # move GCS files to appropriate location base on ingestion output
    # try:
    #     if failure:
    #         for f in config["gcs_staging_files"]:
    #             dest_path = f.replace("/staging/", "/ingested_with_error/")
    #             src_path = f.replace("/staging/", "/tmp/")
    #             gcs_mv(src_gcs_path=src_path, dest_gcs_path=dest_path)
    #     else:
    #         gcs_done_folder = config["gcs_done_folder"]
    #         for f in config["gcs_staging_files"]:
    #             fn = f.split("/")[-1]
    #             dest_path = gcs_done_folder + "/" + fn
    #             src_path = f.replace("/staging/", "/tmp/")
    #             gcs_mv(src_gcs_path=src_path, dest_gcs_path=dest_path)
    # except NotFound:
    #     logger.info("there are no CSV files in GCS tmp folder to move to other foder")


if __name__ == "__main__":
    from rich import print

    move_drive_files_to_dest_folder(
        file_id="1ngHRWhz1_YTw3IB0GtlYqu_0SybJ11dCiw9BMj-0AJg",
        src_folder="https://drive.google.com/drive/folders/19Vc7x2uR_ypDaH2V93yxjx21hDsc3Tbj",
    )

    # logging.basicConfig(level=logging.INFO)
    # from util.dev_config_asset import dev_config
    # from util.config import get_enriched_config

    # # from util.config
    # c = get_enriched_config(dev_config)

    # # test move gcs file
    # now = datetime.utcnow()
    # for f in c["gcs_staging_files"]:
    #     fn = f.split("/")[-1]
    #     folder = f.replace("/staging/", "/done/").replace(fn, "")
    #     folder = Path(folder) / str(now.year) / str(now.month) / str(now.day)
    #     dest_path = str(folder / fn)

    #     f = "gs://logsheet_staging_asia_northeast/staging/site1/awesome_logsheet"
    #     dest_path = (
    #         "gs://logsheet_staging_asia_northeast/staging/site1/awesome_logsheet.csv"
    #     )
    #     src_gcs_path = f.replace("/staging/", "/tmp/")
    #     gcs_mv(src_gcs_path=f, dest_gcs_path=dest_path)

    # gs://logsheet_staging_asia_northeast/done/site1/<year>/<month>/<date>/<filename>

    # test delete gcs file
    # full_gcs_path = "gs://logsheet_staging_asia_northeast/tmp/site1/awesome_logsheet"
    # gcs_rm(full_gcs_path=full_gcs_path)

    # test move drive file
    # c = get_enriched_config(dev_config)
    # print(c)
    # for gsheet in c["gsheet_ids"]:
    #     move_drive_files_to_dest_folder(file_id=gsheet["id"], **c)
