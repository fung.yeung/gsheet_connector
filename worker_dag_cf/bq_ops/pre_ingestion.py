from collections import namedtuple
from datetime import datetime, timedelta
import re
import logging

from google.cloud import bigquery
from google.cloud.bigquery.schema import SchemaField

# import google.auth
logger = logging.getLogger(__name__)


def create_staging_dataset_if_not_exist(
    stg_project_name,
    stg_ds_name,
    sink_location,
    **kwargs,
) -> bigquery.Dataset:
    """create staging dataset in same location as sink dataset if it isn't
    already exist

    Return:
        Staging Dataset Bigquery object
    """
    bq = bigquery.Client(project=stg_project_name)
    # stg_ds_name =
    # f"cn-ops-spdigital.logsheet_staging_{sink_location_display_name}"
    stg_dataset = bigquery.Dataset("{}.{}".format(stg_project_name, stg_ds_name))
    stg_dataset.location = sink_location
    stg_dataset = bq.create_dataset(stg_dataset, exists_ok=True)

    logger.info(
        f'staging dataset "{stg_ds_name}" is available, location: "{sink_location}"'
    )
    return stg_ds_name


def prepare_staging_table(
    stg_bq: bigquery.Client,
    stg_tbl,
    sink_tbl,
    sink_project_name,
    **kwargs,
) -> bigquery.Table:
    """prepare staging table for ingestion from GCS

    1. Drop existing staging table if exists
    2. create empty staging table with latest sink schema
    3. set expirty date on this newly created staging table

    staging table assumption
    1. all datetime columns assumed to be local time (stored as BQ's
       DATETIME data type)
    """
    sink_bq = bigquery.Client(project=sink_project_name)
    sink_tbl_obj = sink_bq.get_table(sink_tbl)
    sink_schema = sink_tbl_obj.schema

    # change "measure_time" from TIMESTAMP to DATETIME as drive logsheet time is
    # in local time
    ts_cols = {
        c.name
        for c in sink_schema
        if c.field_type == "TIMESTAMP" and "insertion_time" not in c.name
    }

    schema_lst = []
    for schema in sink_schema:
        s = schema.to_api_repr()
        if schema.name in ts_cols:
            s["type"] = "DATETIME"
            schema_lst.append(s)
        # elif schema.field_type == 'INTEGER':
        # s['type'] = 'FLOAT'
        # schema_lst.append(s)
        else:
            schema_lst.append(s)

    # add gcs path col for debugging
    gcs_path_col_schema = {"name": "gcs_path", "type": "STRING", "mode": "NULLABLE"}
    schema_lst.append(gcs_path_col_schema)

    # drop staging table manually
    stg_bq.delete_table(stg_tbl, not_found_ok=True)
    logger.info(f'deleted staging table "{stg_tbl}" if exists')

    # recreate staging table
    stg_tbl_obj: bigquery.Table = stg_bq.create_table(
        bigquery.Table(stg_tbl, schema=schema_lst)
    )
    logger.info(
        f'recreated staging table "{stg_tbl}" with sink table\'s "{sink_tbl}" schema'
    )

    # set to expires 5 days later
    n = 5
    n_days_later = datetime.utcnow() + timedelta(days=n)
    stg_tbl_obj.expires = n_days_later
    stg_bq.update_table(stg_tbl_obj, ["expires"])
    logger.info(
        f'set staging table "{stg_tbl}" expiration date to {n} days later - {n_days_later.strftime("%Y-%m-%d")}'
    )


if __name__ == "__main__":
    import sys
    from pathlib import Path
    from rich import print
    import logging

    logging.basicConfig(level=logging.INFO)

    from util.dev_config_asset import action_plan_test, site1
    from util.config import get_enriched_config

    c = get_enriched_config(site1)
    print(c)
    stg_bq = bigquery.Client(project=c["stg_project_name"])
    prepare_staging_table(stg_bq=stg_bq, **c)
    # bq = bigquery.Client()
    # create_staging_dataset_if_not_exist(**c)
    # gcs_to_stg(**c)

    # gcs_to_stg(c)
    # config = get_enriched_config(CONFIG)

    # credentials, _ = google.auth.default(
    #     scopes=[
    #         "https://www.googleapis.com/auth/drive",
    #         "https://www.googleapis.com/auth/bigquery",
    #     ]
    # )
    # # print(c)

    # bigquery_ingest(c, credentials)
