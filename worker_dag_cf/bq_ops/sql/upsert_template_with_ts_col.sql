MERGE
    `{sink_tbl}` AS T
USING
    (
    WITH dedup_with_filename as (
    -- Deduplicate on primary key using GCS filename on descending alphanumeric order
        SELECT 
            *
            , ROW_NUMBER() OVER(
                PARTITION BY
                    {dedup_primary_key}
                ORDER BY
                    gcs_path DESC
            ) AS rn
        FROM
            `{stg_tbl}`
    )    
    SELECT
        * EXCEPT({ts_cols_name}, rn)
        , {convert_ts_cols}
    FROM
        dedup_with_filename
    WHERE
        rn = 1
    ) AS S
ON
    {upsert_primary_key}
WHEN MATCHED THEN
    UPDATE SET
        {update_cols}
WHEN NOT MATCHED THEN
    INSERT VALUES
        ({value_cols})

/* Below are the run time config for last active run
{dag_runtime_config}
*/
;