MERGE
    `{sink_tbl}` AS T
USING
    (
        WITH dedup_add_rn AS (
            SELECT *,
                   ROW_NUMBER() OVER(
                       PARTITION BY {wf_primary_key}
                       ORDER BY {wf_primary_key} DESC
                   ) AS rn
            FROM `{stg_tbl}`
        )
        , dedup AS (
            SELECT *
            FROM dedup_add_rn
            where rn = 1
        )
        SELECT * FROM dedup
    ) AS S
ON
    {primary_key}
WHEN MATCHED THEN
    UPDATE SET
        {sink_tbl_cols}
WHEN NOT MATCHED
    THEN INSERT ROW
;