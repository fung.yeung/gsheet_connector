import logging
import json
import time


from rich import print
from typing import List
from google.cloud import bigquery
from google.cloud.bigquery.job import LoadJob
from google.cloud.bigquery.routine.routine import RoutineType
from google.cloud.bigquery.schema import SchemaField
from google.api_core.exceptions import BadRequest, Forbidden

from bq_ops.pre_ingestion import prepare_staging_table
from util.ingest_output import mv_pending_to_fail

logger = logging.getLogger(__name__)
bq = bigquery.Client()


# TODO:
# 1. in bq.pre_ingestion, a. drop old staging table, b. create new staging
#    table, c. set expiry time
# 2. in bq.ingestion, for each key in "pending_gsheet_ids", call
#    load_table_from_uri for each file. and store everything in a list
# 3. check if all ele in list with .done(), then start adding meta data result
#   a. success -> output_rows
#   b. failure -> i. bad_records
# ii. any other err_message
# 4. remove failure file_id from "pending_gsheet_ids"


def gcs_to_stg(
    site,
    pending_gsheet_ids,
    stg_tbl,
    stg_project_name,
    sink_project_name,
    sink_tbl,
    job_id,
    ingest_output,
    **kwargs,
) -> List[LoadJob]:
    """ingest logsheet CSV from GCS to BigQuery staging dataset in Asia

        1. Fetch sink table schema
        2. Load GCS CSV to Staging table using sink schema

    TODO:
        1. job partial error handling
            a. if len(error) < len(df): return error info in email body
            b. if len(error) = len(df): fail the thing

        email should include clickable link to the file in GCS

    Args:
        config ([type]): [description]
    """

    # create staging table object with same table name as sink table
    stg_bq = bigquery.Client(project=stg_project_name)
    sink_bq = bigquery.Client(project=sink_project_name)
    sink_schema = sink_bq.get_table(sink_tbl).schema

    prepare_staging_table(
        stg_bq=stg_bq,
        stg_tbl=stg_tbl,
        sink_tbl=sink_tbl,
        sink_project_name=sink_project_name,
        stg_project_name=stg_project_name,
    )

    def per_file_gcs_to_stg(
        gsheet_id: dict, gcs_file_path, max_bad_records=1000000
    ) -> bigquery.LoadJob:
        """[summary]

        Args:
            gsheet_id ([type]): [description]
            gcs_file_path ([type]): [description]
            stg_tbl ([type]): [description]
            max_bad_records ([type]): [description]

        Returns:
            [type]: [description]
        """
        # for gsheet in ingest_output[]
        job_config = bigquery.LoadJobConfig(
            skip_leading_rows=1,
            source_format=bigquery.SourceFormat.CSV,
            # create_disposition="CREATE_IF_NEEDED",
            write_disposition="WRITE_APPEND",
            max_bad_records=max_bad_records,
            allow_quoted_newlines=True,
        )

        # f"gs://logsheet_staging_{sink_location_display_name}/staging/{site}/*"
        # source_uri = gcs_staging_files

        job: bigquery.LoadJob = stg_bq.load_table_from_uri(
            source_uris=gcs_file_path,
            destination=stg_tbl,  # tests.<tbl_name>
            job_config=job_config,
            job_id=f"{job_id}_{gsheet_id}_gcs_to_staging",  # Job IDs must be alphanumeric (plus underscores and dashes)
        )

        # if job.
        # break
        # except Forbidden:  # rate limit error when multiple load job point to the same staging table, if this error then just retry after 3 sec
        #     time.sleep(3)
        #     continue
        return job

    # create a load job for each file
    # load_jobs = [
    #     per_file_gcs_to_stg(gsheet_id, gsheet_info["gcs_staging_location"])
    #     for gsheet_id, gsheet_info in pending_gsheet_ids.items()
    # ]

    # load job must be submit one by one, or BQ would report error

    load_jobs = []
    for gsheet_id, gsheet_info in pending_gsheet_ids.items():
        logger.info(f'GCS to BQ - ingesting {gsheet_info["name"]}.csv')
        load_job = per_file_gcs_to_stg(gsheet_id, gsheet_info["gcs_staging_location"])
        load_jobs.append(load_job)
        time.sleep(10)

    # wait for all job to finish processing
    while True:
        if all(j.done() for j in load_jobs):
            break
        time.sleep(1)

    by_gsheet_status = ingest_output["by_gsheet_status"]
    for load_job, gsheet_id in zip(load_jobs, pending_gsheet_ids):
        # curr_gsheet_id_ingest_output = pending_gsheet_ids['pending_gsheet_ids']
        gsheet_status = by_gsheet_status[gsheet_id]
        job_stat = load_job._job_statistics()
        source_uri = load_job.source_uris[0]
        if not load_job.errors:
            logger.info(
                f'SUCCESS - CSV file "{source_uri}" ingested to staging table "{stg_tbl}"'
            )
        else:
            logger.info(
                f'FAIL - CSV file "{source_uri}" fail to ingested to staging table "{stg_tbl}"'
            )
            logger.error(load_job.errors)

            gsheet_status[
                "err_msg"
            ] = load_job.errors  # at most 5 is displayable, limit of the API
            gsheet_status["err_row_cnt"] = int(job_stat.get("badRecords", 0))
            gsheet_status["err_msg"] = load_job.errors
            gsheet_status["err_stage"] = "GCS to BQ Staging"

            mv_pending_to_fail(gsheet_id=gsheet_id, ingest_output=ingest_output)
        gsheet_status["staging_num_rows"] = load_job.output_rows

    # when all is completed, fetch the result and write it to ingest_output

    # wait for job to finish, and report any records that cannot be ingested
    # job.result()
    # job_stat = job._job_statistics()
    # if job.errors:
    #     logger.error(job.errors)
    #     ingest_output[
    #         "err_msg"
    #     ] = job.errors  # at most 5 is displayable, limit of the API
    #     ingest_output["staging_num_rows"] = job.output_rows
    #     ingest_output["err_row_cnt"] = job_stat["badRecords"]

    #     if job_stat["badRecords"] == ingest_output["raw_row_cnt"]:
    #         # if all the records are bad records
    #         ingest_output["ingest_state"] = "zero_records_ingested"
    #         ingest_output[
    #             "err_msg"
    #         ] = "None of the records can be ingest to BigQuery staging table. Abort Ingestion"
    #         logger.warning(f"None of the CSV files were ingested, ingestion abandoned")
    #     else:
    #         ingest_output["ingest_state"] = "bad_records"
    #         ingest_output["err_msg"] = "There are at least 1 bad_records"
    #         logger.warning(
    #             f'CSV files "{", ".join(source_uri)}" ingested to staging table "{stg_tbl}".\nBut there are {len(job.errors)} rows NOT INGESTED due to errors'
    #         )
    # else:
    #     logger.info(
    #         f'CSV files "{", ".join(source_uri)}" ingested to staging table "{stg_tbl}"'
    #     )

    # TODO: set staging table expiry date as python api load job config doesn't have
    # this method
    # return job or None
    return ingest_output


# def _gcs_to_stg(
#     gcs_staging_files,
#     stg_tbl,
#     stg_project_name,
#     sink_project_name,
#     sink_tbl,
#     job_id,
#     sink_schema,
#     ingest_output,
#     **kwargs,
# ) -> LoadJob:
#     """ingest logsheet CSV from GCS to BigQuery staging dataset in Asia

#         1. Fetch sink table schema
#         2. Load GCS CSV to Staging table using sink schema

#     TODO:
#         1. job partial error handling
#             a. if len(error) < len(df): return error info in email body
#             b. if len(error) = len(df): fail the thing

#         email should include clickable link to the file in GCS

#     Args:
#         config ([type]): [description]
#     """

#     # create staging table object with same table name as sink table
#     bq = bigquery.Client(project=stg_project_name)
#     sink_bq = bigquery.Client(project=sink_project_name)
#     sink_schema = sink_bq.get_table(sink_tbl).schema

#     # change "measure_time" from TIMESTAMP to DATETIME as drive logsheet time is
#     # in local time
#     ts_cols = {
#         c.name
#         for c in sink_schema
#         if c.field_type == "TIMESTAMP" and "insertion_time" not in c.name
#     }

#     schema_lst = []
#     for schema in sink_schema:
#         s = schema.to_api_repr()
#         # if schema.name == "measure_time":
#         if schema.name in ts_cols:
#             s["type"] = "DATETIME"
#             schema_lst.append(s)
#         else:
#             schema_lst.append(s)

#     # add gcs path col for debugging
#     gcs_path_col_schema = {"name": "gcs_path", "type": "STRING", "mode": "NULLABLE"}
#     schema_lst.append(gcs_path_col_schema)

#     # drop staging table manually
#     bq.delete_table(stg_tbl, not_found_ok=True)

#     job_config = bigquery.LoadJobConfig(
#         skip_leading_rows=1,
#         source_format=bigquery.SourceFormat.CSV,
#         create_disposition="CREATE_IF_NEEDED",
#         # write_disposition="WRITE_TRUNCATE",
#         schema=schema_lst,  # sink_schema,
#         max_bad_records=ingest_output["raw_row_cnt"],
#         # max_bad_records=1000000,
#         allow_quoted_newlines=True,
#     )

#     # source_uri =
#     # f"gs://logsheet_staging_{sink_location_display_name}/staging/{site}/*"
#     source_uri = gcs_staging_files

#     job = bq.load_table_from_uri(
#         source_uris=source_uri,
#         destination=stg_tbl,  # tests.<tbl_name>
#         job_config=job_config,
#         job_id=f"{job_id}_gcs_to_staging",  # Job IDs must be alphanumeric (plus underscores and dashes)
#     )

#     # wait for job to finish, and report any records that cannot be ingested
#     job.result()
#     job_stat = job._job_statistics()
#     if job.errors:
#         logger.error(job.errors)
#         ingest_output[
#             "err_msg"
#         ] = job.errors  # at most 5 is displayable, limit of the API
#         ingest_output["staging_num_rows"] = job.output_rows
#         ingest_output["err_row_cnt"] = job_stat["badRecords"]

#         if job_stat["badRecords"] == ingest_output["raw_row_cnt"]:
#             # if all the records are bad records
#             ingest_output["ingest_state"] = "zero_records_ingested"
#             ingest_output[
#                 "err_msg"
#             ] = "None of the records can be ingest to BigQuery staging table. Abort Ingestion"
#             logger.warning(f"None of the CSV files were ingested, ingestion abandoned")
#         else:
#             ingest_output["ingest_state"] = "bad_records"
#             ingest_output["err_msg"] = "There are at least 1 bad_records"
#             logger.warning(
#                 f'CSV files "{", ".join(source_uri)}" ingested to staging table "{stg_tbl}".\nBut there are {len(job.errors)} rows NOT INGESTED due to errors'
#             )
#     else:
#         logger.info(
#             f'CSV files "{", ".join(source_uri)}" ingested to staging table "{stg_tbl}"'
#         )

#     # TODO: set staging table expiry date as python api load job config doesn't have
#     # this method
#     return job or None


def get_upsert_statment(
    site: str,
    stg_tbl: str,
    sink_tbl: str,
    sink_schema: dict,
    timezone: str,
    primary_key: List[str],
    config,
    **kwargs,
) -> str:
    """Dynamically generate BigQuery MERGE statment for upsert from staging
    table to sink table

            1. fetch relvant template string from BQ sproc `util.upsert_template`
            2. convert all TIMESTAMP type cols from sink tbl in staging table, from DATETIME to timezone as stated in config
                timezone UTC
            3. format primary_key and sink_tbl_cols (in UPDATE SET section)
            4. join everything together and return nicely formated upsert statment

        Args:
            stg_tbl (str): [description]
            sink_tbl (str): [description]
            primary_key (str): [description]
            template_no (str, optional): [description]. Defaults to None.

        Returns:
            str: [description]
    """

    dedup_primary_key = [f"`{c}`" for c in primary_key]
    dedup_primary_key = "\n\t, ".join(dedup_primary_key)

    upsert_primary_key = [f"T.`{c}` = S.`{c}`" for c in primary_key]
    upsert_primary_key = "\n\tAND ".join(upsert_primary_key)

    schema_raw = bq.get_table(sink_tbl).schema
    schema = [c for c in schema_raw if "insertion_time" not in c.name]

    # udpate cols formatting
    update_cols = [
        f"`{c.name}` = S.`{c.name}`" for c in schema if "insertion_time" not in c.name
    ]
    update_cols.append("insertion_time = CURRENT_TIMESTAMP()")
    update_cols = "\n\t\t, ".join(update_cols)

    # insert cols formatting
    # insert_cols = [f"{c.name}" for c in schema]
    # insert_cols = "\n\t\t, ".join(insert_cols)
    value_cols = [f"S.`{c.name}`" for c in schema if "insertion_time" not in c.name]
    value_cols.append("CURRENT_TIMESTAMP()")
    value_cols = "\n\t\t, ".join(value_cols)

    para = {
        "site": site,
        "sink_tbl": sink_tbl,
        "stg_tbl": stg_tbl,
        # "ts_cols_name": ts_cols_name,
        # "convert_ts_cols": convert_ts_cols,
        "dedup_primary_key": dedup_primary_key,
        "upsert_primary_key": upsert_primary_key,
        "update_cols": update_cols,
        "value_cols": value_cols,
        "dag_runtime_config": json.dumps(config, indent=2),
    }

    # get sink table TIMESTAMP column, as staging table assumed to have all
    # datetime in local time as stated in config
    ts_cols = [c for c in schema if c.field_type == "TIMESTAMP"]

    if ts_cols:
        template_to_use = "upsert_template_with_ts_col.sql"

        ts_cols_name = [f"`{c.name}`" for c in ts_cols]
        ts_cols_name = "\n\t, ".join(ts_cols_name)

        convert_ts_cols = [
            f"TIMESTAMP(`{c.name}`, '{timezone}') AS `{c.name}`" for c in ts_cols
        ]
        convert_ts_cols = "\n\t\t, ".join(convert_ts_cols)

        para["ts_cols_name"] = ts_cols_name
        para["convert_ts_cols"] = convert_ts_cols
    else:
        template_to_use = "upsert_template_without_ts_col.sql"

    with open(f"bq_ops/sql/{template_to_use}") as f:
        upsert_statment_template = f.read()

    output = upsert_statment_template.format(**para)

    logger.info(f"upsert statement generated:")
    logger.info(f"upsert statement generated: {output}")

    return output


def create_or_replace_upsert_sporc(
    upsert_statment, stg_project_name, stg_ds_name, site, **kwargs
):
    """create store procedure for

    Args:
        sproc_body ([type]): [description]
        stg_project_name ([type]): [description]
        stg_ds_name ([type]): [description]
        site ([type]): [description]
    """

    # routine name using YAML KEY name is good enough, adding trailing sink_tbl
    # name to make it all related objects searchable
    routine_name = f"{stg_project_name}.{stg_ds_name}.{site}_{kwargs['sink_tbl_name']}"
    bq.delete_routine(routine_name, not_found_ok=True)
    routine = bigquery.Routine(
        routine_ref=routine_name,
        type_="PROCEDURE",
        body=upsert_statment,
        # description=json.dumps(config, indent=2)
        description="programatically generated for ingestion",
    )
    bq.create_routine(routine, exists_ok=True)
    logger.info(f"upsert statement sproc created: {routine_name}")


def run_upsert_query(upsert_statement, job_id, **kwargs):

    bq = bigquery.Client()
    job = bq.query(upsert_statement, job_id=job_id)

    # if upsert statment has duplicated primary key, BigQuery will fail the job
    try:
        job.result()
        logger.info(f"executed upsert statement")
    except BadRequest:
        logger.info("there was error in upsert SQL execution")
        logger.error(job.errors)

    return job


if __name__ == "__main__":

    from rich import print
    from util.dev_config_asset import action_plan_test, site1, showcase
    from util.config import get_enriched_config
    from util.ingest_output import get_init_ingest_output
    from datetime import datetime

    # logging.basicConfig(level=logging.INFO)

    # config = get_enriched_config(showcase)
    # ingest_output = get_init_ingest_output(config)
    # print(ingest_output)
    # a = {
    #     "11hCBWvfZOw2hZt_qSvoTOJRfXKd6pegeJtuUmbhN2CU": {
    #         "name": "Copy of Copy of Logsheet-IlsanPaikHospital-20211222-20211229_20211230083412",
    #         "gcs_staging_location": "gs://logsheet_staging_asia_northeast/staging/kr/showcase/Copy of Copy of Logsheet-IlsanPaikHospital-20211222-20211229_20211230083412.csv",
    #     }
    # }
    upsert_str = get_upsert_statment(
        site="ghg_performance",
        stg_tbl="cn-ops-spdigital.logsheet_staging_asia_northeast.ghg_performance",
        sink_tbl="cn-ops-spdigital.tests.ghg_performance",
        sink_schema={
            "activity": "STRING",
            "activity_type": "STRING",
            "asia_code": "STRING",
            "business_line": "STRING",
            "business_unit": "STRING",
            "comment": "STRING",
            "country": "STRING",
            "data_owner_email": "STRING",
            "date": "DATE",
            "file_id": "STRING",
            "formula": "FLOAT",
            "global_report_code": "STRING",
            "globalreport_id": "STRING",
            "indicator_name": "STRING",
            "indicator_type": "TIMESTAMP",
            "insertion_time": "TIMESTAMP",
            "interaction_w_other_reports": "STRING",
            "is_applicable": "STRING",
            "period": "STRING",
            "project_name": "STRING",
            "raw_data_availability": "STRING",
            "scope": "STRING",
            "sheet_name": "STRING",
            "site_name": "STRING",
            "sub_activity": "STRING",
            "time_step": "STRING",
            "unit": "STRING",
            "value": "FLOAT",
        },
        timezone="Asia/Hong_Kong",
        primary_key=[
            "globalreport_id",
            "business_unit",
            "project_name",
            "site_name",
            "asia_code",
            "global_report_code",
            "indicator_name",
            "indicator_type",
            "date",
        ],
        config={},
    )
    print(upsert_str)
    # gcs_to_stg(
    #     pending_gsheet_ids=a,
    #     stg_tbl=config["stg_tbl"],
    #     stg_project_name=config["stg_project_name"],
    #     sink_project_name=config["sink_project_name"],
    #     sink_tbl=config["sink_tbl"],
    #     job_id=config["job_id"],
    #     ingest_output=ingest_output,
    # )
    # print(ingest_output)
    # gcs_to_stg(**config)
    # upsert_statment = get_upsert_statment(**config, config=config)
    # print(upsert_statment)
    # create_or_replace_upsert_sporc(upsert_statment, config=config, **config)

# upsert_statment = get_upsert_statment(config=config, **config)
# print(upsert_statment)
# print(upsert_statment)
# create_or_replace_upsert_sporc(upsert_statment, config=config, **config)
# job = run_upsert_query(upsert_statment, **config)
# print(job.result())
# print(job)
