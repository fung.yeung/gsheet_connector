gcloud functions deploy logsheet_ingest_worker \
    --region us-central1 \
    --runtime python39 \
    --entry-point main \
    --trigger-http \
    --service-account=logsheet-ingestion@cn-ops-spdigital.iam.gserviceaccount.com \
    --memory 2048MB \
    --update-labels category=logsheet_ingestion,application=ingestion_worker