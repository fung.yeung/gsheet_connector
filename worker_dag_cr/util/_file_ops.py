from datetime import datetime
import logging
from typing import List
from io import BytesIO

import pandas as pd
import google.auth
from googleapiclient.discovery import build
from google.cloud import storage
from google.api_core.exceptions import NotFound


def create_staging_bucket_if_not_exist(
    bucket_name: str, location: str
) -> storage.Bucket:
    """Get bucket with the same location as sink table dataset, will
    automatically create new one if not exists

        Args:
            config (dict): enriched config with key "sink_location_display_name" and "sink_location"

        Returns:
            [type]: GCS Bucket object
    """
    gcs = storage.Client()
    # bucket_name = f"logsheet_staging_{config['sink_location_display_name']}"

    try:
        bucket = gcs.get_bucket(bucket_name)
    except NotFound:
        # location = config["sink_location"]
        logging.info(f"bucket not found, creating a new one with location:{location}")

        bucket = gcs.create_bucket(
            bucket_name,
            location=location,
        )

    return bucket


def drive_to_gcs(
    drive_file_id: dict, sink_location_display_name: str, gcs_dest_fol: str
) -> None:
    """move google sheet tab "data" to GCS

    Args:
        drive_file_ids (list): [description]
        sink_location_display_name (str): use for deciding which GCS staging
        bucket location to put files in
        gcs_dest_fol (str): [description]
    """
    SCOPES = ["https://www.googleapis.com/auth/drive"]
    credentials, _ = google.auth.default(scopes=SCOPES)
    service = build("drive", "v3", credentials=credentials, cache_discovery=False)
    gsheet_id = drive_file_id["id"]
    gsheet_name = drive_file_id["name"]

    byte_content = (
        service.files()
        .export(
            fileId=gsheet_id,
            mimeType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        )
        .execute()
    )

    df = pd.read_excel(BytesIO(byte_content), sheet_name="data")  # , dtype=str)
    gcs_full_uri = f"gs://logsheet_staging_{sink_location_display_name}/staging/{gcs_dest_fol}/{gsheet_name}"

    # add ingestion metadata
    df["gcs_path"] = gcs_full_uri
    df["insertion_date"] = datetime.utcnow()
    df.to_csv(gcs_full_uri, index=False)

    logging.info(
        'Moved GDrive file "{}" to GCS as "{}"'.format(gsheet_name, gcs_full_uri)
    )


if __name__ == "__main__":
    from rich import print

    logging.basicConfig(level=logging.INFO)
    c = {
        "timezone": "Asia/Hong_Kong",
        "error_email": ["fung.yeung@veolia.com"],
        "gsheet_ids": [
            {
                "id": "1ngHRWhz1_YTw3IB0GtlYqu_0SybJ11dCiw9BMj-0AJg",
                "name": "site1_logsheet1",
            },
            {
                "id": "17UzmAQHsDI9kLaeEzB6mN25QTSXQEBNUr3_R4jHk6TA",
                "name": "site1_logsheet2",
            },
        ],
        "primary_key": ["measure_time", "measure_point"],
        "sink_tbl": "cn-ops-spdigital.tests.data_asia_raw_site2",
        "src_folder": "https://drive.google.com/drive/folders/1X0K8_32VGnCcEJSPLovE1E4JqzmFMrKv",
        "site": "site2",
        "sink_project_name": "cn-ops-spdigital",
        "sink_ds_name": "tests",
        "sink_tbl_name": "data_asia_raw_site2",
        # 'stg_bq': <google.cloud.bigquery.client.Client object at 0x7fc082fc7df0>,
        # 'sink_bq': <google.cloud.bigquery.client.Client object at 0x7fc082fc7fa0>,
        "sink_location": "asia-northeast1",
        "sink_location_display_name": "asia_northeast",
    }
    site = c["site"]
    for gsheet_id in c["gsheet_ids"]:
        drive_to_gcs(
            drive_file_id=gsheet_id,
            gcs_dest_fol=site,
            sink_location_display_name=c["sink_location_display_name"],
        )
