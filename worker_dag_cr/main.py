import logging
import traceback
from flask import Flask, request

app = Flask(__name__)

from google.api_core.exceptions import Forbidden, NotFound

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
# from google.cloud import bigquery
# import google.auth

from file_ops.pre_ingestion import (
    create_staging_bucket_if_not_exist,
    batch_move_drive_to_gcs,
)
from bq_ops.pre_ingestion import create_staging_dataset_if_not_exist
from bq_ops.ingestion import (
    gcs_to_stg,
    get_upsert_statment,
    create_or_replace_upsert_sporc,
    run_upsert_query,
)
from file_ops.after_ingestion import (
    after_ingestion_file_cleanup,
)  # move_drive_files_to_dest_folder, gcs_mv, gcs_rm
from util.config import get_enriched_config
from util.ingest_output import get_init_ingest_output


@app.route("/", methods=["POST"])
def main(**kwargs):
    """main is just a wrapper to put the entire functrion
    "ingest_drive_logsheet" in a try/except block so that any uncatered ingest error
    can return error response 500 for downstream email notfication in airlfow

        Args:
            request ([type], optional): [description]. Defaults to None.
    """
    if request is None:  # dev
        # raw_config: dict = kwargs["config"]
        raw_config: dict = kwargs
    else:
        raw_config: dict = request.get_json()

    print(raw_config)
    logger.info(f"current run config key - {raw_config['site']}")

    # return whatever status code if it is a successful ingestion / catered
    # error, otherwise return 500 and the error message
    try:
        config = get_enriched_config(raw_config)
        logger.info(config)
        ingest_output = get_init_ingest_output(config)
        print(ingest_output)
        output: tuple = ingest_drive_logsheet(
            config=config, ingest_output=ingest_output
        )

    except Forbidden:  # no access right to bigquery object
        err_trackback = traceback.format_exc()
        logger.error(
            "access right not given to ingestion SA for one of the component, ingestion failed"
        )
        logger.error(err_trackback)
        if "ingest_output" not in locals():
            ingest_output = {}
        ingest_output["ingest_state"] = "forbidden"
        ingest_output["ingest_state"] = "notfound"
        ingest_output["err_msg"] = err_trackback

        output = ingest_output, 404

    except Exception:
        err_trackback = traceback.format_exc()

        logger.error(
            "cloud function crashed, error catpched in CF and probably not GCP related, ingestion failed"
        )
        logger.error(err_trackback)

        # it is possible that error has een introduced before ingest_output is instantitated
        if "ingest_output" not in locals():
            ingest_output = {}

        ingest_output["ingest_state"] = "cloud_function_error_general"
        ingest_output["err_msg"] = err_trackback

        output = ingest_output, 500

    finally:

        # handle gdrive and gcs files base on ingestion status / output
        if not ingest_output.get("config"):
            ingest_output["config"] = raw_config

        # need to handle file movement in ALL cases to stop repeating same ingestion
        after_ingestion_file_cleanup(ingest_output=output[0])

    return output


# def ingest_drive_logsheet(request=None, **kwargs):
def ingest_drive_logsheet(config: dict, ingest_output: dict):
    """ingest logsheet from gdrive to bigquery

        1. drive_to_gcs: move gsheet(s) from Drive to GCS
        2. gcs_to_stg: start BQ load job to ingest using GCS uris
        3. run_upsert_query: bring data from BQ staging table to sink table

        para <ingest_output> is a free / mutable variable that most
        sub-procedure have interaction with. Primarily for writing the
        sub-procedure run state as well as the error received from GCP's side.

    Args:
        request ([type], optional): [description]. Defaults to None.

    Returns:
        Response 200: ingestion completed without any error
        Response 207: ingestion completed, those there is error.
        Response 400: There is critical ingestion error, error type are
        catergorized in <ingest_state>, exact error message are in <err_msg>
        function - error mapping:
            1. drive_to_gcs: 1. dtype_conversion_error, 2. mismatch_num_of_cols
            2. gcs_to_stg: 1. zero_records_ingested, 2. bad_records
            3. run_upsert_query/main: 1. upsert_error
    """

    # TODO flask requst JSON to dict
    # if request is None:  # dev
    #     raw_config: dict = kwargs["config"]
    # else:
    #     raw_config: dict = request.get_json()
    # log config so in GCP logging explorer it is possible to trace which config
    # is running

    # gcs: prepare staging bucket in the same location as sink table
    bucket_name = "logsheet_staging_{}".format(config["sink_location_display_name"])
    location = config["sink_location"]
    create_staging_bucket_if_not_exist(bucket_name=bucket_name, location=location)

    # transfer file from gdrive to gcs
    batch_move_drive_to_gcs(
        gsheet_tab_name=config["gsheet_tab_name"],
        gcs_dest_fol=config["gcs_stg_folder"],
        # sink_location_display_name=config["sink_location"],
        sink_schema=config["sink_schema"],
        csv_date_format=config.get("csv_date_format"),
        ingest_output=ingest_output,
    )

    # check if all pending gsheet_ids were in fail_files, if so there is no ponit in continue ingestion
    # if not len(ingest_output["by_gsheet_status"]):
    if len(ingest_output["fail_files"]) == len(config["gsheet_ids"]):
        ingest_output["ingest_state"] = "ingestion_aborted_at_drive_to_gcs"
        return ingest_output, 400

    # ingest from gcs to bigquery staging dataset
    create_staging_dataset_if_not_exist(**config)

    load_job = gcs_to_stg(**config, ingest_output=ingest_output)

    # if there are faile_files AND all files err_row_cnt is equal to gsheet_row_cnt, i.e. nothing got ingested, then stop the ingestion program
    if ingest_output["fail_files"]:
        if all(
            gsheet_info["err_row_cnt"] == gsheet_info["gsheet_row_cnt"]
            for gsheet_info in ingest_output["fail_files"].values()
        ):
            ingest_output["ingest_state"] = "ingestion_aborted_at_gcs_to_stg"
            return ingest_output, 400

    # upsert from staging dataset to sink dataset
    upsert_statement = get_upsert_statment(config=config, **config)
    create_or_replace_upsert_sporc(upsert_statment=upsert_statement, **config)

    # run upsert statment, if error stop function and return info for DAG to
    # send failure email
    upsert_job = run_upsert_query(upsert_statement=upsert_statement, **config)
    if upsert_job.errors:
        # for f in config["gcs_staging_files"]:
        #     gcs_rm(f)

        ingest_output["ingest_state"] = "upsert_error"
        ingest_output["err_msg"] = upsert_job.errors
        return ingest_output, 400
    else:
        ingest_output["run_stat"] = {}

    # by this point by-files ingestion ends, so consider everything in by_gsheet_status as success
    ingest_output["success_files"] = ingest_output["by_gsheet_status"]
    del ingest_output["by_gsheet_status"]

    # Ingestion process reach the end gracefully
    if ingest_output.get("ingest_state"):
        # There is ingestion error, whether it is partial or all row can't ingest will need to handled in airflow DAG
        return (
            ingest_output,
            207,
        )

    raw_number_of_gsheet = len(config["pending_gsheet_ids"])
    if raw_number_of_gsheet == len(ingest_output["fail_files"]):
        ingest_state_msg = "ingestion_fail"
    elif len(ingest_output["fail_files"]) >= 1:
        ingest_state_msg = "partial_success"
    else:
        ingest_state_msg = "completed_without_error"

    ingest_output["ingest_state"] = ingest_state_msg
    # on success records?
    #     CSV # of row
    # staging # of row
    # sink table # of row
    # 	updated row
    # 	inserted row
    # upsert query size billed
    # upsert query cost
    return ingest_output, 200


if __name__ == "__main__":
    import os

    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
    # from rich import print
    # from util.dev_config_asset import (
    #     enriched_dev_config,
    #     action_plan_test,
    #     site1,
    #     showcase,
    # )
    # from util.config import get_enriched_config
    # import flask

    # r = main(config=site1)
    # # print(r)
