from ast import Str
from datetime import datetime
import io

import traceback
import logging

logger = logging.getLogger(__name__)
# from typing import List
# from io import BytesIO
import os

import requests
import pandas as pd

import google.auth
from googleapiclient.discovery import build
from google.cloud import storage
from google.api_core.exceptions import NotFound
from googleapiclient.http import MediaIoBaseDownload
from google.auth.transport.requests import Request


class ColumnsMismatchError(Exception):
    pass


def create_staging_bucket_if_not_exist(
    bucket_name: str, location: str
) -> storage.Bucket:
    """Get bucket with the same location as sink table dataset, will
    automatically create new one if not exists

        Args:
            config (dict): enriched config with key "sink_location_display_name" and "sink_location"

        Returns:
            [type]: GCS Bucket object
    """
    gcs = storage.Client()
    # bucket_name = f"logsheet_staging_{config['sink_location_display_name']}"

    try:
        bucket = gcs.get_bucket(bucket_name)
        logger.info(f"existing bucket found: {bucket_name}")
    except NotFound:
        # location = config["sink_location"]
        logger.info(f"bucket not found, creating a new one with location:{location}")

        bucket = gcs.create_bucket(
            bucket_name,
            location=location,
        )

    return bucket


def get_pandas_dtype(sink_schema: dict):
    """when pandas convert gsheet to CSV, there are multiple issues that need to
    handled before ingesting to BigQuery, all these are gsheet related and thus
    should be handled independently from the rest of the ingestion process

    if sink table columns dtype is:
        1. BOOLEAN, INTEGER
            > dataframe column should be INT
        2. DECIMAL / FLOAT64
            > float
        3. TIMESTAMP
            > datetime64

        Args:
            sink_schema (dict):
    """

    bigquery_to_pandas = {
        "TIMESTAMP": "datetime64",
        "STRING": "object",
        "FLOAT": "float64",
        "NUMERIC": "float64",
        "BOOLEAN": "bool",
        "INTEGER": "Int64",  # cater for NA value in gsheet as well
    }

    return {c: bigquery_to_pandas[ctype] for c, ctype in sink_schema.items()}


def get_schema_diff_err_msg(gsheet_schema: list, sink_schema: dict):
    """get the exact schema different between source gsheet and sink table, to
    be included in error email notification

        Args:
            gsheet_schema (list): [description]
            sink_schema (dict): [description]

        Returns:
            [str]: [description]
    """
    err_msg = []
    if "insertion_time" in sink_schema:
        del sink_schema["insertion_time"]
    if gsheet_extra_col := (gsheet_schema - sink_schema.keys()):
        err_msg.append(f"columns present only in gsheet: {', '.join(gsheet_extra_col)}")

    if sink_table_extra_col := (sink_schema.keys() - gsheet_schema):
        err_msg.append(
            f"columns present only in bq sink table: {', '.join(sink_table_extra_col)}"
        )

    output = "\n".join(err_msg)
    print(output)
    return output


def _get_drive_file_obj(
    drive_file_id: str,
    drive_file_mime_type: str,
) -> io.BytesIO:
    """Read drive file as File Obj, there are two type of possible input with different handling of reading from drive into dataframe
        1. google spreadsheet: export using export_links with "requests", drive API v3 has 10MB limit
        2. excel files: export using drive API v3, since these are binary file (instead of google documents), that use "get_media" method

    Args:
        drive_file_id (str): _description_
        drive_file_mime_type (str): _description_

    Returns:
        pd.DataFrame: _description_
    """
    SCOPES = [
        "https://www.googleapis.com/auth/drive",
        "https://www.googleapis.com/auth/spreadsheets",
        "https://www.googleapis.com/auth/cloud-platform",
    ]
    credentials, _ = google.auth.default(scopes=SCOPES)
    if credentials.token is None:
        credentials.refresh(Request())

    service = build("drive", "v3", credentials=credentials, cache_discovery=False)

    logger.info("getting file object from gdrive")
    if drive_file_mime_type == "application/vnd.google-apps.spreadsheet":
        logger.info("file mimetype is google spreadsheet")
        gsheet_export_link = f"https://docs.google.com/spreadsheets/export?id={drive_file_id}&exportFormat=xlsx"
        headers = {"Authorization": "Bearer " + credentials.token}
        r = requests.get(gsheet_export_link, headers=headers)
        r.raise_for_status()
        df_byte_content = r.content
        df_file_obj = io.BytesIO(df_byte_content)

    elif drive_file_mime_type in [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
        "application/vnd.ms-excel",
        "application/vnd.ms-excel.sheet.macroEnabled.12",
        "text/csv",
    ]:  # download excel file directly
        logger.info("file mimetype is Excel")
        df_file_obj = io.BytesIO()
        request = service.files().get_media(fileId=drive_file_id)
        downloader = MediaIoBaseDownload(df_file_obj, request)
        done = False
        while done is False:
            _, done = downloader.next_chunk()
            # each chunk is download in df_file_obj
    else:
        raise ValueError(
            "unrecognized mime_type, file is neither Excel nor Google Spreadsheet"
        )

    logger.info("fetched file object from gdrive")

    return df_file_obj


def _drive_to_gcs_csv(
    drive_file_id: str,
    drive_filename: str,
    drive_file_mime_type: str,
    gsheet_tab_name: str,
    gcs_dest_fol: str,
    sink_schema: dict,
    csv_date_format: str,
    **kwargs,
):
    """move csv file to GCS, trandform date format so that it is iso format that
    is ingestable by BQ

    Args:
        drive_file_id (str): _description_
        drive_filename (str): _description_
        drive_file_mime_type (str): _description_
        gsheet_tab_name (str): _description_
        gcs_dest_fol (str): _description_
        sink_schema (dict): _description_
    """
    gsheet_name = drive_filename
    gcs_staging_uri = f"{gcs_dest_fol}/{gsheet_name}.csv"

    logger.info(
        'Begining to convert GDrive Excel File "{}" to GCS as "{}"'.format(
            gsheet_name, gcs_staging_uri
        )
    )

    drive_file_obj = _get_drive_file_obj(
        drive_file_id=drive_file_id, drive_file_mime_type=drive_file_mime_type
    )
    # read_csv need to parse date column properly, to write it back as standard
    drive_file_obj.seek(0)

    date_cols = [
        c
        for c, dtype in sink_schema.items()
        if dtype in ("TIMESTAMP", "DATE", "DATETIME") and c != "insertion_time"
    ]

    if date_cols:

        cols = [c for c in sink_schema if c != "insertion_time"]
        logger.info(f"date columns present: date_format is {csv_date_format}")
        df = pd.read_csv(
            filepath_or_buffer=drive_file_obj,
            names=cols,
            header=0,
            dtype=pd.StringDtype(),
        )
        # df = pd.read_csv(
        #     filepath_or_buffer=drive_file_obj, header=0, dtype=pd.StringDtype()
        # )
        for date_col in date_cols:
            df[date_col] = pd.to_datetime(
                df[date_col], format=csv_date_format, errors="coerce"
            )  # errors to cater for null value
    else:
        df = pd.read_csv(
            filepath_or_buffer=drive_file_obj, header=0, dtype=pd.StringDtype()
        )

    gcs_tmp_uri = f"{gcs_dest_fol.replace('/staging/', '/tmp/')}/{gsheet_name}.csv"

    # raw data for backup
    df.to_csv(gcs_tmp_uri, index=False)

    # actual ingestion file
    # add ingestion metadata, fn / ingestion time for staging table
    df["insertion_date"] = datetime.utcnow()
    df["gcs_path"] = gcs_staging_uri
    df.to_csv(gcs_staging_uri, index=False)  # , quoting=csv.QUOTE_ALL)

    logger.info(
        'Moved GDrive Excel file "{}" to GCS as "{}"'.format(
            gsheet_name, gcs_staging_uri
        )
    )

    return len(df)


def _drive_to_gcs_excel(
    drive_file_id: str,
    drive_filename: str,
    drive_file_mime_type: str,
    gsheet_tab_name: str,
    gcs_dest_fol: str,
    sink_schema: dict,
):
    """This is probably not working yet!!!
    convert Excel file from Gdrive to CSV in GCS

    Args:
        drive_file_id (str): _description_
        drive_filename (str): _description_
        drive_file_mime_type (str): _description_
        gsheet_tab_name (str): _description_
        gcs_dest_fol (str): _description_
        sink_schema (dict): _description_
    """
    gsheet_name = drive_filename
    gcs_staging_uri = f"{gcs_dest_fol}/{gsheet_name}.csv"

    logger.info(
        'Begining to convert GDrive Excel File "{}" to GCS as "{}"'.format(
            gsheet_name, gcs_staging_uri
        )
    )

    drive_file_obj = _get_drive_file_obj(
        drive_file_id=drive_file_id, drive_file_mime_type=drive_file_mime_type
    )

    df = pd.read_excel(io=drive_file_obj, sheet_name=gsheet_tab_name, dtype="string")

    # Pandas sometime read excel Integer as Float (even with dtype defined as
    # str / object as above, so apply nullable dtype to put remove any trailing
    # decimal for interger type and any other error)
    df.convert_dtypes()

    gcs_tmp_uri = f"{gcs_dest_fol.replace('/staging/', '/tmp/')}/{gsheet_name}.csv"

    # raw data for backup
    df.to_csv(gcs_tmp_uri, index=False)

    # actual ingestion file
    # add ingestion metadata, fn / ingestion time for staging table
    df["insertion_date"] = datetime.utcnow()
    df["gcs_path"] = gcs_staging_uri
    df.to_csv(gcs_staging_uri, index=False)  # , quoting=csv.QUOTE_ALL)

    logger.info(
        'Moved GDrive Excel file "{}" to GCS as "{}"'.format(
            gsheet_name, gcs_staging_uri
        )
    )

    return len(df)


def _drive_to_gcs_spreadsheet(
    drive_file_id: str,
    drive_filename: str,
    drive_file_mime_type: str,
    gsheet_tab_name: str,
    gcs_dest_fol: str,
    sink_schema: dict,
    **kwargs,
) -> int:
    """move google sheet data content to GCS as CSV

        possible ingestion errors:
            1. dtype_conversion_error
            2. mismatch_num_of_cols

        hard-coded (necessary) transformation:
        1. column "insertion_time" will be added
        2. column "gcs_path" will be added (only to staging files /staging, not
           the bak files in /tmp)
        3. if "timezone" exists in gsheet, will be deleted for staging files

    Args:
        drive_file_ids (list): [description]
        sink_location_display_name (str): use for deciding which GCS staging
        bucket location to put files in
        gcs_dest_fol (str): [description]

    Returns:
        len(df) (int): number of rows in gsheet (exclude header)
    """

    # SCOPES = [
    #     "https://www.googleapis.com/auth/drive",
    #     "https://www.googleapis.com/auth/spreadsheets",
    #     "https://www.googleapis.com/auth/cloud-platform",
    # ]
    # credentials, _ = google.auth.default(scopes=SCOPES)
    # service = build("drive", "v3", credentials=credentials, cache_discovery=False)
    # gsheet_id = drive_file_id["id"]
    # gsheet_name = drive_file_id["name"]
    # gsheet_id = drive_file_id
    gsheet_name = drive_filename
    gcs_staging_uri = f"{gcs_dest_fol}/{gsheet_name}.csv"

    logger.info(
        'Begining to move GDrive Google Spreadsheet File "{}" to GCS as "{}"'.format(
            gsheet_name, gcs_staging_uri
        )
    )

    # byte_content = (
    #     service.files()
    #     .export(
    #         fileId=gsheet_id,
    #         mimeType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    #     )
    #     .execute()
    # )
    # dtype = get_pandas_dtype(sink_schema=sink_schema)
    # if dtype.get("insertion_time"):
    #     del dtype["insertion_time"]

    # 1. Read_excel and explicity apply INTEGER datatype, otherwise it will be
    # read as float64
    # 2. Read boolean field purposely as "float64", in order to cater for both
    #    FALSE and N/A value in logsheet, then cast it back to boolean to export
    # as CSV
    # dtype_mapping = {
    #     "INTEGER": "Int64",
    #     "BOOLEAN": "float64",
    #     "DATE": "datetime64",
    #     "TIMESTAMP": "datetime64",
    #     "STRING": object,
    # }
    # dtype = {c: dtype_mapping[col_dtype] for c, col_dtype in
    # sink_schema.items()}
    # dtype = dict()

    # for c, col_dtype in sink_schema.items():
    #     if col_dtype == "INTEGER":
    #         dtype[c] = "Int64"
    #     elif col_dtype == "BOOLEAN":
    #         dtype[c] = "float64"
    #     elif col_dtype == "DATE" or col_dtype == "TIMESTAMP":
    #         dtype[c] = "datetime64"
    #     elif col_dtype == "STRING":
    #         dtype[c] = object

    drive_file_obj = _get_drive_file_obj(
        drive_file_id=drive_file_id, drive_file_mime_type=drive_file_mime_type
    )

    df = pd.read_excel(
        io=drive_file_obj,
        sheet_name=gsheet_tab_name,
        # dtype=dtype,
        # dtype=pd.StringDtype(),
        # names=col_names,
    )

    # check if column number matches between gsheet AND sink_table, if it
    # doesn't then stop the ingestion
    gsheet_schema = [c for c in df.columns if "timezone" not in c]
    num_of_gsheet_cols = len(gsheet_schema)
    num_of_sink_tbl_cols = (
        len(sink_schema) - 1
    )  # minus 1 column "insertion_time" from sink table
    if num_of_gsheet_cols != num_of_sink_tbl_cols:

        schema_err_msg = get_schema_diff_err_msg(
            gsheet_schema=gsheet_schema, sink_schema=sink_schema
        )
        # output["ingest_state"] = "mismatch_num_of_cols"
        # output["err_msg"] = schema_err_msg
        logger.error("mismatch in schema between gsheet and sink_tbl")
        logger.error(schema_err_msg)
        raise ColumnsMismatchError(schema_err_msg)

    # skip all blank row
    df = df.dropna(how="all")

    # cast bool fields back to boolean
    bool_cols = [c for c, col_dtype in sink_schema.items() if col_dtype == "BOOLEAN"]

    for bool_col in bool_cols:
        df[bool_col] = df[bool_col].astype("boolean")

    # read_excel read all numbers as float if there is NA, manually cast integer
    # column back to INT and catering NA value
    int_cols = [c for c, dt in sink_schema.items() if dt == "INTEGER"]
    for int_col in int_cols:
        df[int_col] = df[int_col].astype("Int64")

    # cast sink table "DATE", "TIMESTAMP" field to date, otherwise rows can be
    # interpret with inconsistency
    date_cols = [
        c
        for c, dt in sink_schema.items()
        if dt in ("DATE", "TIMESTAMP") and c != "insertion_time"
    ]

    for date_col in date_cols:
        df[date_col] = pd.to_datetime(df[date_col])

    # cast sink table "TIMESTAMP" field to TIMESTAMP

    # cast dataframe columns to the dtype as in BigQuery, this avoid pandas
    # assigning the wrong dtype and thus raise unknown error down the road
    # try:
    #     df = df.astype(dtype=dtype)  # , errors="ignore")
    # except TypeError as e:
    #     err_msg = str(e)
    #     ingest_output["ingest_state"] = "dtype_conversion_error"
    #     ingest_output["err_msg"] = str(e)
    #     logger.error(
    #         "There is error when converting gsheet dtype using sink_tbl data type"
    #     )
    #     logger.error(
    #         f"error gsheet url: https://docs.google.com/spreadsheets/d/{gsheet_id}"
    #     )
    #     logger.error(err_msg)
    #     return

    # row count to check if error
    # cummulative_row_cnt = ingest_output.get("row_cnt", 0)
    # ingest_output["total_raw_row_cnt"] = cummulative_row_cnt + len(df)
    # output["row_cnt"] = len(df)

    gcs_tmp_uri = f"{gcs_dest_fol.replace('/staging/', '/tmp/')}/{gsheet_name}.csv"

    # raw data for backup
    df.to_csv(gcs_tmp_uri, index=False)  # , quoting=csv.QUOTE_ALL)

    # actual ingestion file
    # group's logsheet file comes with field "timezone", this is now defined in
    # YAML file and no longer need it for ingestion
    if "timezone" in df.columns:
        del df["timezone"]

    # add ingestion metadata, fn / ingestion time for staging table
    df["insertion_date"] = datetime.utcnow()
    df["gcs_path"] = gcs_staging_uri
    df.to_csv(gcs_staging_uri, index=False)  # , quoting=csv.QUOTE_ALL)

    logger.info(
        'Moved GDrive Google Spreadsheet file "{}" to GCS as "{}"'.format(
            gsheet_name, gcs_staging_uri
        )
    )

    return len(df)


def batch_move_drive_to_gcs(
    gsheet_tab_name,
    gcs_dest_fol,
    sink_schema,
    csv_date_format,
    ingest_output: dict,
    **kwargs,
):
    """Move each gsheet files from drive to gcs, tracking each gsheet file
    status in the progress in ingest_output

        Spreadsheet and excel requires different treatment, hence two method
        with same signature but different behaviour

    Args:
        gsheet_ids ([type]): [description]
    """
    pending_gsheet_ids = ingest_output["by_gsheet_status"]

    for gsheet_id, gsheet_info in pending_gsheet_ids.items():
        try:
            drive_fn = gsheet_info["name"]
            mime_type = gsheet_info["mime_type"]
            drive_to_gcs_dispatch_mapping = {
                "application/vnd.google-apps.spreadsheet": _drive_to_gcs_spreadsheet,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": _drive_to_gcs_excel,
                "application/vnd.ms-excel.sheet.binary.macroEnabled.12": _drive_to_gcs_excel,
                "application/vnd.ms-excel": _drive_to_gcs_excel,
                "application/vnd.ms-excel.sheet.macroEnabled.12": _drive_to_gcs_excel,
                "text/csv": _drive_to_gcs_csv,
            }

            _drive_to_gcs_func = drive_to_gcs_dispatch_mapping.get(mime_type)
            logging.info(
                f"drive_to_gcs function dispatched is {_drive_to_gcs_func.__name__}"
            )

            if _drive_to_gcs_func is None:
                raise ValueError("file type are not CSV or Excel or Google Spreadsheet")

            row_cnt = _drive_to_gcs_func(
                drive_file_id=gsheet_id,
                drive_filename=drive_fn,
                drive_file_mime_type=mime_type,
                gsheet_tab_name=gsheet_tab_name,
                gcs_dest_fol=gcs_dest_fol,
                sink_schema=sink_schema,
                csv_date_format=csv_date_format,
            )

            gsheet_info["gsheet_row_cnt"] = row_cnt
            # ingest_output["success_files"][gsheet_id] = gsheet_info
        except ColumnsMismatchError as e:
            # add failure reason
            gsheet_info["err_type"] = "mismatch_in_columns"
            ingest_output["fail_files"][gsheet_id] = gsheet_info

        except Exception as e:  # for unknown error
            err_msg = traceback.format_exc()
            print(err_msg)
            gsheet_info["err_type"] = "unknown_error"
            gsheet_info["err_msg"] = err_msg
            ingest_output["fail_files"][gsheet_id] = gsheet_info

    # remove this gsheet file from staging files list as it never get to
    # GCS staging (can't delete during the loop)
    failed_keys = pending_gsheet_ids.keys() & ingest_output["fail_files"].keys()
    if failed_keys:
        new_pending_gsheet_ids = {
            k: v for k, v in pending_gsheet_ids.items() if k not in failed_keys
        }
        ingest_output["by_gsheet_status"] = new_pending_gsheet_ids


if __name__ == "__main__":
    from rich import print

    logging.basicConfig(level=logging.INFO)
    # from util.dev_config_asset import site1, refential
    # from util.config import get_enriched_config
    # from util.ingest_output import get_init_ingest_output

    # config = get_enriched_config(site1)
    # ingest_output = get_init_ingest_output(config)
    # gcs_dest_fol = config["gcs_stg_folder"]
    # sink_schema = config["sink_schema"]
    # gsheet_tab_name = config["gsheet_tab_name"]
    # print(config)

    # sc = {
    #     "zone_number": "STRING",
    #     "contract_account_number": "STRING",
    #     "consumer_name": "STRING",
    #     "installation_number": "STRING",
    #     "consumer_class": "STRING",
    #     "consumer_category": "STRING",
    #     "consumer_type_tag": "STRING",
    #     "consumer_full_address": "STRING",
    #     "telephone_number": "STRING",
    #     "telephone_land_number": "STRING",
    #     "current_meter_serial_no": "STRING",
    #     "meter_owner": "STRING",
    #     "meter_brand": "STRING",
    #     "coefficient_factor_of_meter": "INTEGER",
    #     "connection_date": "DATE",
    #     "tap_size": "INTEGER",
    #     "number_of_residential_flat": "INTEGER",
    #     "number_of_non_residential_flat": "INTEGER",
    #     "govt_flag": "STRING",
    #     "gis_consumer_id": "STRING",
    #     "demo_zone_flag": "STRING",
    #     "esr_description": "STRING",
    #     "mru": "STRING",
    #     "walk_id": "STRING",
    #     "meter_reader_name": "STRING",
    #     "creation_date_in_sap": "DATE",
    #     "re_connection_date": "STRING",
    #     "status": "STRING",
    #     "disconnected_date": "DATE",
    #     "insertion_time": "TIMESTAMP",
    # }
    # _drive_to_gcs_csv(
    #     # drive_file_id="1HvaULfxTisuDaT3kwBFdCtJ4b0SuKZre", # meter_data
    #     drive_file_id="1jHPS2CwXgaVhrO3fiYyB4yYc_uIrBPP-",  # consumption_data
    #     # drive_file_id="1GEw-S5vhkXkIgNDY_ePZEy4GaFQzlLpG",
    #     drive_filename="consumption_data_csv",
    #     drive_file_mime_type="text/csv",
    #     gsheet_tab_name="Sheet1",
    #     gcs_dest_fol="gs://logsheet_staging_asia_northeast/dev",
    #     sink_schema=consumption_schema,
    #     csv_date_format="%d-%m-%Y",
    # )

    _drive_to_gcs_spreadsheet(
        # drive_file_id="1HvaULfxTisuDaT3kwBFdCtJ4b0SuKZre", # meter_data
        drive_file_id="1LYM_sAb43n6dtjjEw-nk9wFEM96zKUeinrrZdwOoCIU",
        drive_filename="debug-bioconversion",
        drive_file_mime_type="application/vnd.google-apps.spreadsheet",
        gsheet_tab_name="data",
        gcs_dest_fol="gs://logsheet_staging_asia_northeast/dev",
        sink_schema={
            "process_stage": "STRING",
            "trial_code": "STRING",
            "feedstock_code": "STRING",
            "bsf_strain": "STRING",
            "experimental_setup_date": "DATE",
            "larvae_age": "STRING",
            "treatment_control": "STRING",
            "replicates": "INTEGER",
            "measure_point": "STRING",
            "measure_unit": "STRING",
            "measure_value": "FLOAT",
            "insertion_time": "TIMESTAMP",
        },
    )
    # print(ingest_output)
    # https://docs.google.com/spreadsheets/d/19XBSsrCAjkBnUX8xe0vpKck46m5d2SNcCwKX3CRHkZQ/edit#gid=810043373

    # _drive_to_gcs(drive_file_id=, drive_filename=, drive_file_mime_type=, gsheet_tab_name=, sink_location_display_name=, gcs_dest_fol=,sink_schema=)
    # batch_move_drive_to_gcs(
    #     pending_gsheet_ids=config["pending_gsheet_ids"],
    #     gsheet_tab_name=config["gsheet_tab_name"],
    #     gcs_dest_fol=config["gcs_stg_folder"],
    #     sink_location_display_name=config["sink_location"],
    #     ingest_output=ingest_output,
    #     sink_schema=config["sink_schema"],
    # )
    # print(ingest_output)
    # get_schema_diff(None, None)
    # target_gsheet = site1
    # c = get_enriched_config(target_gsheet)
    # print(c)
    # location = c["sink_location_display_name"]
    # bucket_name = f"logsheet_staging_{location}"
    # create_staging_bucket_if_not_exist(
    #     bucket_name=bucket_name, location=c["sink_location"]
    # )

    # gcs_dest_fol = c["gcs_stg_folder"]
    # location = c["sink_location"]
    # sink_schema = c["sink_schema"]

    # for gsheet in c["gsheet_ids"]:
    #     drive_to_gcs(
    #         sink_location_display_name=c["sink_location_display_name"],
    #         drive_file_id=gsheet,
    #         gcs_dest_fol=gcs_dest_fol,
    #         ingest_output={},
    #         sink_schema=sink_schema,
    #     )
