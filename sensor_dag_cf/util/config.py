from typing import Dict, List

from google.cloud import storage
import yaml


def _get_gcs_configs() -> List[dict]:
    gcs = storage.Client()
    bucket = gcs.get_bucket("all_config")

    yaml_blobs = [
        b
        for b in bucket.list_blobs(prefix="logsheet_ingestion")
        if b.name != "logsheet_ingestion/"
    ]

    configs_by_region = []

    for b in yaml_blobs:
        bu = b.name.replace("logsheet_ingestion/", "").split("_")[0]
        yaml_content = bucket.get_blob(b.name).download_as_string()
        yaml_d = yaml.safe_load(yaml_content)
        # yaml_d["default_args"]["bu"] = bu
        configs_by_region.append(yaml_d)

    return configs_by_region


def _get_local_configs() -> List[dict]:
    with open("util/yaml_config.yaml") as f:
        yaml_d = yaml.safe_load(f)

    return [yaml_d]  # in prod it is a list of config by region


def get_raw_configs(dev_mode=False) -> List[dict]:
    """get all yaml configs in gs://dag_config/logsheet_ingestion/*

    Returns:
        List[dict]: list of dict, each element in list is a region / yaml file
    """
    # gcs = storage.Client()
    # bucket = gcs.get_bucket("all_config")

    # yaml_blobs = [
    #     b
    #     for b in bucket.list_blobs(prefix="logsheet_ingestion")
    #     if b.name != "logsheet_ingestion/"
    # ]

    # configs_by_region = []

    # for b in yaml_blobs:
    #     bu = b.name.replace("logsheet_ingestion/", "").split("_")[0]
    #     yaml_content = bucket.get_blob(b.name).download_as_string()
    #     yaml_d = yaml.safe_load(yaml_content)
    #     # yaml_d["default_args"]["bu"] = bu
    #     configs_by_region.append(yaml_d)

    # return configs_by_region
    return _get_local_configs() if dev_mode else _get_gcs_configs()


def config_apply_default(raw_config: dict) -> Dict[str, dict]:
    """pass in config and apply default args to rest of the keys

    Returns:
        dict: config with all the static information, pending dynamic
        information on DagRun, i.e. gsheet_ids
    """

    default_args = raw_config.pop("default_args")

    output = dict()
    for site, site_config in raw_config.items():
        default = default_args.copy()
        default.update(site_config)
        output[site] = default

    return output


def get_config(dev_mode=False) -> List[Dict]:
    """

    Returns:
        [type]: [description]
    """
    return [config_apply_default(c) for c in get_raw_configs(dev_mode=dev_mode)]


if __name__ == "__main__":
    from rich import print

    print(get_config(False))
    # print(*configs)
    # c = chain(configs)
    # for item in c:
    #     print(item)

    # d = {
    #     "default_args": {
    #         "error_email": ["fung.yeung@veolia.com"],
    #         "primary_key": ["measure_time", "measure_point"],
    #         "dest_folder": "https://drive.google.com/drive/folders/19Vc7x2uR_ypDaH2V93yxjx21hDsc3Tbj",
    #     },
    #     "site1": {
    #         "src_folder": "https://drive.google.com/drive/folders/1KDxtR14hAuXsbazZYmDiqaX2LiZwsqyx",
    #         "sink_tbl": "cn-ops-spdigital.tests.data_asia_raw_site1",
    #     },
    #     "site2": {
    #         "src_folder": "https://drive.google.com/drive/folders/1X0K8_32VGnCcEJSPLovE1E4JqzmFMrKv",
    #         "sink_tbl": "cn-ops-spdigital.tests.data_asia_raw_site2",
    #     },
    #     "site3": {
    #         "src_folder": "https://drive.google.com/drive/folders/1TMDNlOXI76t4tdyg-NhNf2FXoA5u5THh",
    #         "sink_tbl": "cn-ops-spdigital.tests.data_asia_raw_type_A",
    #         "primary_key": ["measure_time", "measure_point", "site"],
    #     },
    #     "bu": "dev",
    # }
    # default_args = d.pop("default_args")
    # print(default_args)
    # print(d.values())
    # default_args.update(d.values())
