from typing import List
from fnmatch import fnmatch

import google.auth
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


def filter_gsheet_fn(gsheets: List[dict], fn_pattern: str) -> List[dict]:
    """given a list of dict, return only gsheet filename that matches fn_pattern specificied in YAML config

    Args:
        gsheets (List[dict]): _description_
        fn_pattern (str): if YAML doesn't include any special character, this will be fn_prefix, otherwise UNIX glob pattern.
    """
    # if ('*' not in fn_pattern)
    #
    glob_characters = ["*", "?", "[", "]"]
    if not any(char in fn_pattern for char in glob_characters):
        fn_pattern = fn_pattern + "*"

    return [gsheet for gsheet in gsheets if fnmatch(gsheet["name"], fn_pattern)]


def get_gsheet_id(folder_id, filter_fn_pattern=None) -> List[dict]:
    """Fetch all gsheet_ids given a folder_id, will not raise error if it is

    Args:

    Returns:
        list of gsheet file_id in the folder

    Ref:
        Drive API v3
        https://developers.google.com/resources/api-libraries/documentation/drive/v3/python/latest/drive_v3.files.html"""

    # if run locally need to set env var GOOGLE_APPLICATION_CREDENTIALS to service
    # acc ount keyfile, gcloud with personal account doesn't work
    SCOPES = ["https://www.googleapis.com/auth/drive"]
    credentials, _ = google.auth.default(scopes=SCOPES)

    service = build("drive", "v3", credentials=credentials)

    # only search for spreadsheet or xlsx / xlsb / xls / xlsm excel files
    query_string = f"""parents='{folder_id}' and
        (
            mimeType = 'application/vnd.google-apps.spreadsheet'
            or mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            or mimeType = 'application/vnd.ms-excel.sheet.binary.macroEnabled.12'
            or mimeType = 'application/vnd.ms-excel'
            or mimeType = 'application/vnd.ms-excel.sheet.macroEnabled.12'
            or mimeType = 'text/csv'
        )
    """

    try:
        results = (
            service.files()
            .list(
                pageSize=None,  # maximum is 1000 records/files in a folder, which shouldn't be possible
                fields="files(id, name, mimeType,exportLinks)",
                q=query_string,
            )
            .execute()
        )
    except HttpError as e:  # invalid folder_id
        if e.status_code == 404:
            # if "403" in str(e):
            #     print(
            #         f"ingestion program service account denied access to folder {folder_id}"
            #     )
            print(f"can't find folder_id '{folder_id}', skipping this folder")
        return

    # stop function, no need for fn filter and export link extraction
    if results is None:
        return []

    gsheet_ids = results.get("files", [])

    # filter gsheet result if provided from YAML config
    if filter_fn_pattern:
        gsheet_ids = filter_gsheet_fn(gsheets=gsheet_ids, fn_pattern=filter_fn_pattern)

    # export link extraction
    # for gsheet_id in gsheet_ids:
    #     # print(i["exportLinks"])
    #     if exportLinks := gsheet_id.get("exportLinks"):
    #         gsheet_id["export_link"] = exportLinks[
    #             "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    #         ]
    #         del gsheet_id["exportLinks"]

    return gsheet_ids
    # return [i['export_link']i['exportLinks']['text/csv'] for i in items]
    # if items:
    #     return [item for item in items]


if __name__ == "__main__":
    from rich import print

    folder_ids = [
        # "1KDxtR14hAuXsbazZYmDiqaX2LiZwsqyx",  # site1
        # "19Vc7x2uR_ypDaH2V93yxjx21hDsc3Tbj"
        # "1X0K8_32VGnCcEJSPLovE1E4JqzmFMrKv",  # site2
        # "1TMDNlOXI76t4tdyg-NhNf2FXoA5u5THh",  # site3
        "19o7wy0WuPGfCNZDw2hF82YxuXQ9COcM5"
    ]

    pattern = "testing_sheet"
    for folder_id in folder_ids:
        output = get_gsheet_id(folder_id=folder_id)  # , filter_fn_pattern="Consum*")
        # if pattern:
        #     output = filter_gsheet_fn(output, pattern)
        print(output)
