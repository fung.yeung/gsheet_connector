from rich import print
import flask

from util.config import get_config
from util.drive import get_gsheet_id


def get_dag_config(request: flask.Request = None, **kwargs):
    """Read, transform and produce read to use config by DAG

    1. read YAML config and apply default args to each site
    2. For each folder with new logsheet file, append logsheet ids list into
       site's config

    Args:
        request ([type], optional): [description]. Defaults to None.

    Returns:
        dict: dag config to be pass to worker dag
    """

    if kwargs.get("config"):
        # configs: dict = kwargs["config"]
        configs = kwargs.get("config")
        # print(configs)
    else:
        configs: dict = get_config()

    # configs = get_config()
    site_with_new_file = []  # here site means YAML key
    site_with_no_files = []
    for config in configs:  # for config in each region/yaml file
        # print(config)
        for site, site_config in config.items():
            folder_id = site_config["src_folder"].replace(
                "https://drive.google.com/drive/folders/", ""
            )

            # dev
            # folder_id = "19Vc7x2uR_ypDaH2V93yxjx21hDsc3Tbj"
            # site_config["filter_fn_pattern"] = "testing"
            gsheet_ids = get_gsheet_id(
                folder_id=folder_id,
                filter_fn_pattern=site_config.get("filter_fn_pattern"),
            )

            # empty folder then continue with next site in the region's config
            # otherwise append gsheet_ids to config the pass to worker DAG
            if not gsheet_ids:
                site_with_no_files.append(site)
                continue
            else:
                site_config["site"] = site
                site_with_new_file.append(site_config)
                site_config["gsheet_ids"] = gsheet_ids

        # output = {k: v for k, v in config.items() if k in site_with_new_file}
    # print(f"Following YAML keys has no new files: {', '.join(site_with_no_files)}")
    # print(f"Following YAML keys has new files:: {', '.join(site_with_new_file)}")n
    # print(output)
    print(site_with_new_file)
    return None or flask.jsonify(site_with_new_file)


if __name__ == "__main__":

    config = [
        {
            "test_fung": {
                "error_email": ["fung.yeung@veolia.com"],
                "primary_key": ["measure_time", "measure_point"],
                "dest_folder_success": "https://drive.google.com/drive/folders/1zNb83aUrRnChsejfXu83i_crKleQ0vhE",
                "dest_folder_failure": "https://drive.google.com/drive/folders/1W8s_bg-n0_Y9xuonh8aoqUSQDHF7OPX3",
                "bu": "kr",
                "timezone": "Asia/Seoul",
                "mail_on_success": True,
                "gsheet_tab_name": "data",
                "src_folder": "https://drive.google.com/drive/folders/1cN5jFVSwWkDeabYhwfGNx0Wxy1SvE2hv",
                "sink_tbl": "cn-ops-spdigital.tests.uat_site_1"
                # "filter_fn_pattern": ",
            }
        },
    ]
    print(get_dag_config(config=None))
    # config = None
    # print(get_dag_config(config=config))
    # print(get_dag_config(dev_mode=False))
    # test
