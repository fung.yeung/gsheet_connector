gcloud functions deploy logsheet_config_getter \
    --region us-central1 \
    --runtime python39 \
    --entry-point get_dag_config \
    --trigger-http \
    --service-account=logsheet-ingestion@cn-ops-spdigital.iam.gserviceaccount.com \
    --memory 256MB \
    --update-labels category=logsheet_ingestion,application=ingestion_sensor